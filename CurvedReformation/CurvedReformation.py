import vtk
from vtk.util import numpy_support
from vtk.util.vtkAlgorithm import VTKPythonAlgorithmBase


class MyFilter(VTKPythonAlgorithmBase):
    # TODO Unstable Version...!! There are many many many ~~~ parts of hard-coding ...

    """
    This filter is to convert PolyData to flatten vtkImageData
    """

    def __init__(self):
        VTKPythonAlgorithmBase.__init__(self,
                                        nInputPorts=1, inputType='vtkPolyData',
                                        nOutputPorts=1, outputType='vtkImageData')

    def RequestData(self, request, inInfo, outInfo):
        info = inInfo[0].GetInformationObject(0)
        input = vtk.vtkPolyData.GetData(info)

        narray = numpy_support.vtk_to_numpy(input.GetPointData().GetArray(0))
        # TODO why array order is (z, y, x) ...??? .. let me see..... hm....
        narray = narray.reshape([140, 1, 500], order='F')
        vtk_scalars = numpy_support.numpy_to_vtk(narray.ravel(), deep=True, array_type=vtk.VTK_DOUBLE)

        info = outInfo.GetInformationObject(0)

        output = vtk.vtkImageData.GetData(info)
        output.SetDimensions(500, 1, 140)
        output.GetPointData().SetScalars(vtk_scalars)

        return 1


def SweepLine(line, direction, distance, cols):
    rows = line.GetNumberOfPoints()
    spacing = distance / cols
    surface = vtk.vtkPolyData()

    # Generate the points
    cols += 1

    numberOfPoints = rows * cols
    numberOfPolys = (rows - 1) * (cols - 1)
    points = vtk.vtkPoints()
    points.Allocate(numberOfPoints)
    polys = vtk.vtkCellArray()
    polys.Allocate(numberOfPolys * 4)

    x = [0] * 3
    cnt = 0
    for row in range(rows):
        for col in range(cols):
            p = [0] * 3
            line.GetPoint(row, p)
            x[0] = p[0] + direction[0] * col * spacing
            x[1] = p[1] + direction[1] * col * spacing
            x[2] = p[2] + direction[2] * col * spacing
            points.InsertPoint(cnt, x)
            cnt += 1

    # Generate the quads
    pts = [0] * 4
    for row in range(rows - 1):
        for col in range(cols - 1):
            pts[0] = col + row * cols
            pts[1] = pts[0] + 1
            pts[2] = pts[0] + cols + 1
            pts[3] = pts[0] + cols
            polys.InsertNextCell(4, pts)

    surface.SetPoints(points)
    surface.SetPolys(polys)

    return surface


# source
reader = vtk.vtkDICOMImageReader()
reader.SetDirectoryName(r'/Users/scott/Dicom/1')
reader.Update()
print('reader : ', reader.GetOutput())

# Read the Polyline
# polyLineReader = vtk.vtkPolyDataReader()
# polyLineReader.SetFileName('./polyline.vtk')
# polyLineReader.Update()
# Create Polyline through data spec
def make_polyline(_P):
    points = vtk.vtkPoints()
    for p in _P:
        points.InsertNextPoint(p)
    polyline = vtk.vtkPolyLine()
    polyline.GetPointIds().SetNumberOfIds(len(P))
    for i in range(len(P)):
        polyline.GetPointIds().SetId(i, i)
    cells = vtk.vtkCellArray()
    cells.InsertNextCell(polyline)
    polyData = vtk.vtkPolyData()
    polyData.SetPoints(points)
    polyData.SetLines(cells)
    return polyData

P = [[52.80917, 74.87506, 0],
     [64.32781, 116.37835, 0],
     [86.53604, 138.93358, 0],
     [96.25214, 168.77589, 0],
     [106.66225, 177.45098, 0],
     [122.97142, 176.75697, 0],
     [138.23957, 148.99668, 0],
     [159.05979, 114.29633, 0],
     [171.37218, 76.19243, 0]
     ]
polyData = make_polyline(P)


pano_width = 500
pano_height = 140

spline = vtk.vtkSplineFilter()
# spline.SetInputConnection(polyLineReader.GetOutputPort())
spline.SetInputData(polyData)
spline.SetSubdivideToSpecified()
spline.SetNumberOfSubdivisions(pano_width-1)

# Sweep the line to form a surface
# Coronal
direction = [0.0, 0.0, 1.0]
distance = pano_height

spline.Update()

surface = SweepLine(spline.GetOutput(), direction, distance, pano_height-1)
# print('surface !!! : ', surface)

# Probe the volume with the extruded surface
sampleVolume = vtk.vtkProbeFilter()
sampleVolume.SetInputConnection(1, reader.GetOutputPort())
sampleVolume.SetInputData(0, surface)
sampleVolume.Update()

# s = MySource()
f = MyFilter()
f.SetInputConnection(sampleVolume.GetOutputPort())
f.Update()
print('Output!!! : ', f.GetOutputDataObject(0))

# window/level..
wlLut = vtk.vtkWindowLevelLookupTable()
s = reader.GetOutput().GetScalarRange()[0]
e = reader.GetOutput().GetScalarRange()[1]
windowing_range = 500
windowing_level = 20
wlLut.SetWindow(windowing_range)
wlLut.SetLevel(windowing_level)

# data extent
(xMin, xMax, yMin ,yMax, zMin, zMax) = reader.GetExecutive().GetWholeExtent(reader.GetOutputInformation(0))
(xSpacing, ySpacing, zSpacing) = reader.GetOutput().GetSpacing()
(x0, y0, z0) = reader.GetOutput().GetOrigin()

# center of volume
center = [int(pano_width/2), 0, int(pano_height/2)]

# set cutting plane
# Coronal
viewUp = [0, 0, -1]
i, j, k = 0, yMax, 0

# Create a mapper and actor
mapper = vtk.vtkDataSetMapper()
mapper.SetInputConnection(f.GetOutputPort())
mapper.SetLookupTable(wlLut)
mapper.SetScalarRange(s, e)

actor = vtk.vtkActor()
actor.SetMapper(mapper)

# Create a renderer, render window, and interactor
renderer = vtk.vtkRenderer()
renderWindow = vtk.vtkRenderWindow()
renderWindow.AddRenderer(renderer)
renderWindowInteractor = vtk.vtkRenderWindowInteractor()
renderWindowInteractor.SetRenderWindow(renderWindow)
istyle = vtk.vtkInteractorStyleUser()
renderWindowInteractor.SetInteractorStyle(istyle)

# Add the actors to the scene
renderer.AddActor(actor)
renderer.SetBackground(.2, .3, .4)

# Set the camera for viewing medical images
renderer.GetActiveCamera().SetFocalPoint(center)
renderer.GetActiveCamera().SetPosition(center[0]+i, center[1]+j, center[2]+k)
renderer.GetActiveCamera().SetViewUp(viewUp)

# Mouse Event
def MouseWheelEvent(_iren, _event):

    _thick = 1

    if _event == r'MouseWheelForwardEvent':
        _thick = 1
    elif _event == r'MouseWheelBackwardEvent':
        _thick = -1

    for p in P:
        p[1] += _thick

    polyData = make_polyline(P)
    spline.SetInputData(polyData)
    spline.Update()
    surface = SweepLine(spline.GetOutput(), direction, distance, pano_height - 1)
    sampleVolume.SetInputData(0, surface)
    sampleVolume.Update()
    f.Update()
    mapper.Update()
    renderer.Render()
    renderWindowInteractor.Render()

renderWindowInteractor.AddObserver('MouseWheelForwardEvent', MouseWheelEvent)
renderWindowInteractor.AddObserver('MouseWheelBackwardEvent', MouseWheelEvent)

# Render and interact
renderWindow.Render()
renderWindowInteractor.Start()
