import vtk

# source
reader = vtk.vtkDICOMImageReader()
reader.SetDirectoryName(r'/Users/scott/Dicom/1')
reader.Update()

c_f, o_f = vtk.vtkColorTransferFunction(), vtk.vtkPiecewiseFunction()
p = vtk.vtkVolumeProperty()
m, v = vtk.vtkSmartVolumeMapper(), vtk.vtkVolume()

opacityWindow = 4096
opacityLevel = 1048

# mapper
m.SetInputConnection(reader.GetOutputPort())
m.SetBlendModeToMaximumIntensity()

# property
p.SetIndependentComponents(True)
c_f.AddRGBSegment(0.0, 1.0, 1.0, 1.0, 255.0, 1.0, 1.0, 1.0)
o_f.AddSegment(opacityLevel - 0.5*opacityWindow, 0.0, opacityLevel + 0.5*opacityWindow, 1.0)
p.SetColor(c_f)
p.SetScalarOpacity(o_f)
p.SetInterpolationTypeToLinear()

# actor
v.SetMapper(m)
v.SetProperty(p)

# renderer
ren = vtk.vtkRenderer()
renWin = vtk.vtkRenderWindow()
renWin.AddRenderer(ren)
iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow(renWin)
istyle = vtk.vtkInteractorStyleTrackballCamera()
iren.SetInteractorStyle(istyle)

# camera
camera = ren.GetActiveCamera()
c = v.GetCenter()
camera.SetFocalPoint(c)
camera.SetPosition(c[0], c[1]+400, c[2])
camera.SetViewUp(0, 0, -1)

ren.AddViewProp(v)
renWin.Render()
iren.Initialize()
iren.Start()
