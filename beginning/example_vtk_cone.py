import vtk

# Source
s = vtk.vtkConeSource()

# Mapper
m = vtk.vtkPolyDataMapper()
m.SetInputConnection(s.GetOutputPort())

# Actor
a = vtk.vtkActor()
a.SetMapper(m)

# Render
ren = vtk.vtkRenderer()
renWin = vtk.vtkRenderWindow()
renWin.AddRenderer(ren)
iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow(renWin)
ren.AddViewProp(a)

# Render Start
renWin.Render()
iren.Initialize()
iren.Start()
