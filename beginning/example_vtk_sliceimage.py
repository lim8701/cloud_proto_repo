import vtk

# source
reader = vtk.vtkDICOMImageReader()
reader.SetDirectoryName(r'/Users/scott/Dicom/1')
reader.Update()

p = vtk.vtkImageProperty()
m, a = vtk.vtkImageResliceMapper(), vtk.vtkImageSlice()
plane = vtk.vtkPlane()

# data extent
(xMin, xMax, yMin ,yMax, zMin, zMax) = reader.GetExecutive().GetWholeExtent(reader.GetOutputInformation(0))
(xSpacing, ySpacing, zSpacing) = reader.GetOutput().GetSpacing()
(x0, y0, z0) = reader.GetOutput().GetOrigin()

# center of volume
center = [x0 + xSpacing * 0.5 * (xMin + xMax),
          y0 + ySpacing * 0.5 * (yMin + yMax),
          z0 + zSpacing * 0.5 * (zMin + zMax)]

# set cutting plane
plane.SetOrigin(center)
plane.SetNormal(0, 0, 1)
viewUp = [0, -1, 0]
i, j, k = 0, 0, -zMax

# mapper
m.SetInputConnection(reader.GetOutputPort())
m.SetSlicePlane(plane)

# property
p.SetColorWindow(2000)
p.SetColorLevel(800)
p.SetAmbient(0.0)
p.SetDiffuse(1.0)
p.SetOpacity(1.0)
p.SetInterpolationTypeToLinear()

# actor
a.SetMapper(m)
a.SetProperty(p)

# renderer
ren = vtk.vtkRenderer()
renWin = vtk.vtkRenderWindow()
renWin.AddRenderer(ren)
iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow(renWin)

# camera
camera = ren.GetActiveCamera()
camera.ParallelProjectionOn()
ren.ResetCameraClippingRange()
camera.SetParallelScale(0.5 * (yMax-yMin+1)*ySpacing)
camera.SetFocalPoint(center)
camera.SetPosition(center[0]+i, center[1]+j, center[2]+k)
camera.SetViewUp(viewUp)

# add actor
ren.AddViewProp(a)

# render start
renWin.Render()
iren.Initialize()
iren.Start()