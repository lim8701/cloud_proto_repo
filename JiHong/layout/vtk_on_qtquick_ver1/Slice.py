from __future__ import division

import vtk
import numpy as np
from vtk_on_qtquick_ver1 import cy_vtk
from PyQt5.QtCore import pyqtSlot


class Slice(cy_vtk.Vtk_image_holder):

    class ORIENTATION:
        """
        asigned value is an up vector of plane
        """
        Sagital = 0
        Coronal = 1
        Axial = 2
        Default = Sagital

    def __init__(self, reader, orientation, *args, **kwds):
        super().__init__(*args, **kwds)

        assert reader
        self.reader = reader

        if orientation == 'coronal':
            self.orientation = self.ORIENTATION.Coronal
        elif orientation == 'sagital':
            self.orientation = self.ORIENTATION.Sagital
        elif orientation == 'axial':
            self.orientation = self.ORIENTATION.Axial
        else:
            self.orientation = self.ORIENTATION.Default

        # initial interval
        self.thickness = 1

        self.mouse_lbtn_pressed = False
        self.mouse_rbtn_pressed = False

        self.vtk_img = None

        self.im = vtk.vtkImageResliceMapper()
        self.ip = vtk.vtkImageProperty()
        self.ia = vtk.vtkImageSlice()
        self.plane = vtk.vtkPlane()

        self.ready()

    def ready(self):
        self.vtk_img = self.reader.get_vtkimage()
        self.ready_for_basic_pipeline()

    def ready_for_basic_pipeline(self):
        self.RX_RY_RZ = self.reader.get_range_of_volume()
        (xMin, xMax, yMin, yMax, zMin, zMax) = self.RX_RY_RZ
        self.SX_SY_SZ = self.reader.get_spacing_of_volume()
        (xSpacing, ySpacing, zSpacing) = self.SX_SY_SZ
        self.OX_OY_OZ = self.reader.get_origin_of_volume()
        (x0, y0, z0) = self.OX_OY_OZ

        center = [x0 + xSpacing * 0.5 * (xMin + xMax),
                  y0 + ySpacing * 0.5 * (yMin + yMax),
                  z0 + zSpacing * 0.5 * (zMin + zMax)]

        self.im.SetInputConnection(self.vtk_img.GetOutputPort())

        vec_plane, vec_view_up, displacement_camera = self.get_vectors_for_camera()
        pos_camera = [sum(x) for x in zip(center, displacement_camera)]
        self.plane.SetNormal(vec_plane)

        self.plane.SetOrigin(center)
        self.im.SetSlicePlane(self.plane)

        self.ip.SetColorWindow(2000)
        self.ip.SetColorLevel(1000)
        self.ip.SetAmbient(0.0)
        self.ip.SetDiffuse(1.0)
        self.ip.SetOpacity(1.0)
        self.ip.SetInterpolationTypeToLinear()

        self.ia.SetMapper(self.im)
        self.ia.SetProperty(self.ip)

        self.camera = self.ren.GetActiveCamera()
        self.camera.ParallelProjectionOn()
        self.ren.ResetCameraClippingRange()
        self.camera.SetParallelScale(0.5 * (yMax - yMin + 1) * ySpacing)
        self.camera.SetFocalPoint(center)
        self.camera.SetPosition(pos_camera)
        self.camera.SetViewUp(vec_view_up)

        style = vtk.vtkInteractorStyleUser()
        self.view.SetInteractorStyle(style)

        self.ren.AddViewProp(self.ia)

        # custom interaction
        self.view.AddObserver("MouseWheelForwardEvent", self.MouseWheelEvent)
        self.view.AddObserver("MouseWheelBackwardEvent", self.MouseWheelEvent)

    def get_vectors_for_camera(self):
        """
        :return: vec_plane, vec_view_up, displacement_camera
        """
        vec_plane = []
        vec_view_up = []
        displacement_camera = []

        (xMin, xMax, yMin, yMax, zMin, zMax) = self.RX_RY_RZ

        if self.orientation == self.ORIENTATION.Sagital:
            vec_plane = [1, 0, 0]
            vec_view_up = [0, 0, 1]
            displacement_camera = [xMax, 0, 0]
        elif self.orientation == self.ORIENTATION.Coronal:
            vec_plane = [0, 1, 0]
            vec_view_up = [0, 0, 1]
            displacement_camera = [0, -yMax, 0]
        elif self.orientation == self.ORIENTATION.Axial:
            vec_plane = [0, 0, 1]
            vec_view_up = [0, -1, 0]
            displacement_camera = [0, 0, -zMax]

        return vec_plane, vec_view_up, displacement_camera

    def MouseWheelEvent(self, iren, event=""):
        self.move_slab_plane_by_scroll(event)

    def move_slab_plane_by_scroll(self, event):
        # calculate Hor/Ver Vector
        normalVector = self.plane.GetNormal()
        upVector = [0, 0, 1] if (self.orientation == self.ORIENTATION.Sagital or
                                 self.orientation == self.ORIENTATION.Coronal) else [0, 1, 0]
        newVec1 = [0] * 3
        vtk.vtkMath.Cross(normalVector, upVector, newVec1)
        newVec2 = [0] * 3
        vtk.vtkMath.Cross(normalVector, newVec1, newVec2)

        if event == r'MouseWheelForwardEvent':
            direction = -1 if self.orientation == self.ORIENTATION.Sagital else 1
        elif event == r'MouseWheelBackwardEvent':
            direction = 1 if self.orientation == self.ORIENTATION.Sagital else -1

        interval = self.thickness * direction

        # generate interval(thick) vector to axis
        interval_mat = [0] * 3
        interval_mat[self.orientation] = 1 * interval

        if self.orientation == self.ORIENTATION.Sagital:
            V = np.array([normalVector, newVec1, newVec2]).transpose()
        elif self.orientation == self.ORIENTATION.Coronal:
            V = np.array([newVec1, normalVector, newVec2]).transpose()
        elif self.orientation == self.ORIENTATION.Axial:
            V = np.array([newVec1, newVec2, normalVector]).transpose()

        newXYZ = np.dot(V, interval_mat)

        self.plane.SetOrigin(self.plane.GetOrigin() + newXYZ)
        self.im.SetSlicePlane(self.plane)

        self.view._RenderWindow.Render()

    @pyqtSlot('int', 'int')
    def on_changed_windowing_level(self, a, b):
        self.ip.SetColorWindow(b - a)
        self.ip.SetColorLevel((a + b) / 2)

        self.view._RenderWindow.Render()
