import os
import sys
from PyQt5.QtWidgets import QApplication
from PyQt5.QtCore import QUrl
from PyQt5.QtQuick import QQuickView
import Reader, Volume, Slice
import cy_vtk


if __name__ == '__main__':
    app = QApplication(sys.argv)

    reader = Reader.Reader('C:/sample_Dicom/')
    volume = Volume.Volume(reader)
    coronal = Slice.Slice(reader, 'coronal')
    sagital = Slice.Slice(reader, 'sagital')
    axial = Slice.Slice(reader, 'axial')
    view = cy_vtk.vtk_on_qtquick()
    view.addVolume('vtk_volume', volume)
    view.addVolume('vtk_coronal', coronal)
    view.addVolume('vtk_sagital', sagital)
    view.addVolume('vtk_axial', axial)
    view.setSource(QUrl.fromLocalFile(os.path.join(os.path.dirname(__file__), 'img_prov.qml')))
    view.initializeSigSlot()
    view.setResizeMode(QQuickView.SizeRootObjectToView)
    view.show()

    sys.exit(app.exec_())
