import QtQuick 2.0


Image {
    /*
    Necessarily, Specify name, source
    name: "vtk_volume"
    source: "image://vtk_volume"
    */

    cache: false

    signal sigMousePressed(var id, var mouse)
    signal sigMouseReleased(var id, var mouse)
    signal sigMouseMoved(var id, var mouse)
    signal sigWheel(var id, var mouse)
    signal sigResize(var id, real width, real height)

    property var name: null

    onWidthChanged: {
        //console.log("width changed", width);
        sigResize(name, width, height);
    }

    onHeightChanged: {
        //console.log("height changed", height);
        sigResize(name, width, height);
    }



    MouseArea {
        acceptedButtons: Qt.AllButtons
        anchors.fill: parent

        onPressed: {
            console.log("parent name", parent.name, parent.width, parent.height)

            var _mouse = {"x": mouse.x, "y": mouse.y, "accepted": mouse.accepted, "button": mouse.button,
                            "buttons": mouse.buttons, "modifiers": mouse.modifiers, "source": mouse.source,
                            "wasHeld": mouse.wasHeld};
            parent.sigMousePressed(parent.name, _mouse);
        }

        onReleased: {
            var _mouse = {"x": mouse.x, "y": mouse.y, "accepted": mouse.accepted, "button": mouse.button,
                            "buttons": mouse.buttons, "modifiers": mouse.modifiers, "source": mouse.source,
                            "wasHeld": mouse.wasHeld};
            parent.sigMouseReleased(parent.name, _mouse);
        }

        onPositionChanged: {
            var _mouse = {"x": mouse.x, "y": mouse.y, "accepted": mouse.accepted, "button": mouse.button,
                            "buttons": mouse.buttons, "modifiers": mouse.modifiers, "source": mouse.source,
                            "wasHeld": mouse.wasHeld};
            parent.sigMouseMoved(parent.name, _mouse);
        }

        onWheel: {
            var _wheel = {"x": wheel.x, "y": wheel.y, "accepted": wheel.accepted, "buttons": wheel.buttons,
                            "modifiers": wheel.modifiers, "angleDelta": wheel.angleDelta, "pixelDelta": wheel.pixelDelta};
            parent.sigWheel(parent.name, _wheel);
        }
    }
}