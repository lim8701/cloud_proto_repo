import vtk
from vtk_on_qtquick_ver1 import cy_vtk


class Volume(cy_vtk.Vtk_image_holder):
    def __init__(self, reader, *args, **kwds):
        super().__init__(*args, **kwds)

        self.vtk_img = None

        assert reader
        self.reader = reader

        # basic component
        self.colorFun = vtk.vtkColorTransferFunction()
        self.opacityFun = vtk.vtkPiecewiseFunction()
        self.property = vtk.vtkVolumeProperty()

        # Smart Volume Mapper
        self.mapper = vtk.vtkSmartVolumeMapper()
        self.mapper.SetRequestedRenderMode(vtk.vtkSmartVolumeMapper.GPURenderMode)

        # Set transfer
        self.window_of_tf = 3070
        self.level_of_tf = 1535

        self.volume = vtk.vtkVolume()

        # Camera
        self.camera = vtk.vtkCamera()

        # init base pipeline
        self.ready()

    def ready(self):
        self.vtk_img = self.reader.get_vtkimage()
        self.ready_for_basic_pipeline()

    def ready_for_basic_pipeline(self):
        self.range_vol = self.reader.get_range_of_volume()

        # Mapper
        self.mapper.SetInputConnection(self.vtk_img.GetOutputPort())

        # Set transfer
        self.set_transfer_preset("MIP")

        self.property.SetIndependentComponents(True)
        self.property.SetColor(self.colorFun)
        self.property.SetScalarOpacity(self.opacityFun)
        self.property.SetInterpolationTypeToLinear()

        # vtkVolume
        self.volume.SetMapper(self.mapper)
        self.volume.SetProperty(self.property)

        # Camera
        ex = self.range_vol
        self.camera = self.ren.GetActiveCamera()
        c = self.volume.GetCenter()
        self.camera.SetFocalPoint(c[0], c[1], c[2])
        self.camera.SetPosition(c[0] + (ex[1] / 4), c[1] - (ex[3]), c[2] + (ex[5] / 2))
        self.camera.SetViewUp(0, 0, 1)

        # Render
        self.ren.AddViewProp(self.volume)

    def set_transfer_preset(self, _type):
        self.colorFun.RemoveAllPoints()
        self.opacityFun.RemoveAllPoints()
        self.property.SetIndependentComponents(True)

        if _type == "MIP":
            self.colorFun.AddRGBSegment(self.level_of_tf - 0.5*self.window_of_tf, 1.0, 1.0, 1.0,
                                        self.level_of_tf + 0.5*self.window_of_tf, 1.0, 1.0, 1.0)
            self.opacityFun.AddSegment(self.level_of_tf - 0.5*self.window_of_tf, 0.0,
                                       self.level_of_tf + 0.5*self.window_of_tf, 1.0)
            self.mapper.SetBlendModeToMaximumIntensity()
            self.property.ShadeOff()

        elif _type == "CompositeRamp":
            self.colorFun.AddRGBSegment(self.level_of_tf - 0.5*self.window_of_tf, 0.0, 0.0, 0.0,
                                        self.level_of_tf + 0.5*self.window_of_tf, 1.0, 1.0, 1.0)
            self.opacityFun.AddSegment(self.level_of_tf - 0.5*self.window_of_tf, 0.0,
                                       self.level_of_tf + 0.5*self.window_of_tf, 1.0)
            self.mapper.SetBlendModeToComposite()
            self.property.ShadeOff()

        elif _type == "CompositeShadeRamp":
            self.colorFun.AddRGBSegment(self.level_of_tf - 0.5 * self.window_of_tf, 0.0, 0.0, 0.0,
                                        self.level_of_tf + 0.5 * self.window_of_tf, 1.0, 1.0, 1.0)
            self.opacityFun.AddSegment(self.level_of_tf - 0.5 * self.window_of_tf, 0.0,
                                       self.level_of_tf + 0.5 * self.window_of_tf, 1.0)
            self.mapper.SetBlendModeToComposite()
            self.property.ShadeOn()

        elif _type == "CT_Skin":
            self.colorFun.AddRGBPoint(-2048, 0, 0, 0, 0.5, 0.0)
            self.colorFun.AddRGBPoint(-500, .62, .36, .18, 0.5, 0.0)
            self.colorFun.AddRGBPoint(0, .88, .60, .29, 0.33, 0.45)
            self.colorFun.AddRGBPoint(2048, .83, .66, 1, 0.5, 0.0)

            self.opacityFun.AddPoint(-2048, 0, 0.5, 0.0 )
            self.opacityFun.AddPoint(-500, 0, 0.5, 0.0 )
            self.opacityFun.AddPoint(0, 1.0, 0.33, 0.45 )
            self.opacityFun.AddPoint(2048, 1.0, 0.5, 0.0)

            self.mapper.SetBlendModeToComposite()
            self.property.ShadeOn()
            self.property.SetAmbient(0.1)
            self.property.SetDiffuse(0.9)
            self.property.SetSpecular(0.2)
            self.property.SetSpecularPower(10.0)
            self.property.SetScalarOpacityUnitDistance(0.8919)

        elif _type == "CT_Bone":
            self.colorFun.AddRGBPoint(-3024, 0, 0, 0, 0.5, 0.0)
            self.colorFun.AddRGBPoint(-16, 0.73, 0.25, 0.30, 0.49, .61)
            self.colorFun.AddRGBPoint(641, .90, .82, .56, .5, 0.0)
            self.colorFun.AddRGBPoint(3071, 1, 1, 1, .5, 0.0)

            self.opacityFun.AddPoint(-3024, 0, 0.5, 0.0)
            self.opacityFun.AddPoint(-16, 0, .49, .61)
            self.opacityFun.AddPoint(641, .72, .5, 0.0)
            self.opacityFun.AddPoint(3071, .71, 0.5, 0.0)

            self.mapper.SetBlendModeToComposite()
            self.property.ShadeOn()
            self.property.SetAmbient(0.1)
            self.property.SetDiffuse(0.9)
            self.property.SetSpecular(0.2)
            self.property.SetSpecularPower(10.0)
            self.property.SetScalarOpacityUnitDistance(0.8919)

        self.view._RenderWindow.Render()

