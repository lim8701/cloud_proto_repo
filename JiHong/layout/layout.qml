import QtQuick 2.0
import QtGraphicalEffects 1.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.3
import "modules/"
import "vtk_on_qtquick_ver1/"

Item{
  id:main_layout
  width: 1300
  height: 900
  Column{
    id: left_layout
    //objectName: "left_layout"
    width: 305
    height: parent.height

    Rectangle{
      width: parent.width
      height: 40
      color: "#262626"

      Text{
        width: parent.width
        height: parent.height
        text: "<i><b>OnDemand</b><u><font color=\"#8CC63F\">3D</font></u> <b>OMNI</b></i>"
        font.bold: true
        //font.weight: Font.Black
        font.pointSize: 18
        color: "#00B0F0"
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment : Text.AlignVCenter
      }
    }

    //TODO module section
    ViewControl{id:rect1}
    GeneralTools{id:rect2}
    ImplantPlacement{
      id:rect3
    }
  }

  TabView {
    id: right_layout
    width: parent.width - left_layout.width
    height: parent.height
    x: left_layout.width

    Tab {
      id: tab1
      title: "<b>Dental</b>"

      GridLayout {
        id: grid
        columns: 2
        rows: 2
        columnSpacing: 1
        rowSpacing : 0

        ColumnLayout{
          id: coronal_layout
          spacing: 0
          Rectangle{
            id: coronal_mouse
            //width:right_layout.width/2
            Layout.fillWidth: true
            height:20
            color:"#282832"
            Text{
              text: "     Coronal"
              color: "white"
              font.family: "Helvetica"
              font.pointSize: 12
              font.weight: Font.Black
              height: parent.height
              verticalAlignment : Text.AlignVCenter
            }

            MouseArea{
              acceptedButtons: Qt.AllButtons
              anchors.fill: parent
              property real change_size: vtk_coronal.width
              //Component.onCompleted: console.log("******************         "+vtk_coronal.width)

              onDoubleClicked:{
                if(vtk_coronal.width <= change_size){
                  sagital_layout.visible = false
                  axial_layout.visible = false
                  volume_layout.visible = false
                  coronal_layout.Layout.maximumHeight = -1
                  change_size = vtk_coronal.width
                }else{
                  coronal_layout.Layout.maximumHeight = 480
                  sagital_layout.visible = true
                  axial_layout.visible = true
                  volume_layout.visible = true
                }
              }
            }
          }
          VtkImage {
            id: vtk_coronal
            name: "vtk_coronal"
            source: "image://" + name
            objectName: name
            Layout.fillHeight: true
            Layout.fillWidth: true
            //Component.onCompleted: console.log(coronal_layout.width + " " + coronal_layout.height)
          }
        }
        ColumnLayout{
          id: sagital_layout
          spacing: 0
          Rectangle{
            id: sagital_mouse
            Layout.fillWidth: true
            height:20
            color:"#282832"
            Text{
              text: "     Sagital"
              color: "white"
              font.family: "Helvetica"
              font.pointSize: 12
              font.weight: Font.Black
              height: parent.height
              verticalAlignment : Text.AlignVCenter
            }

            MouseArea{
              acceptedButtons: Qt.AllButtons
              anchors.fill: parent
              property real change_size: vtk_sagital.width

              onDoubleClicked:{
                if(vtk_sagital.width <= change_size){
                  coronal_layout.visible = false
                  axial_layout.visible = false
                  volume_layout.visible = false
                  sagital_layout.Layout.maximumHeight = -1
                  change_size = vtk_coronal.width
                }else{
                  sagital_layout.Layout.maximumHeight = 480
                  coronal_layout.visible = true
                  axial_layout.visible = true
                  volume_layout.visible = true
                }
              }
            }
          }
          VtkImage {
            id: vtk_sagital
            name: "vtk_sagital"
            source: "image://" + name
            objectName: name
            Layout.fillHeight: true
            Layout.fillWidth: true
          }
        }
        ColumnLayout{
          id: axial_layout
          spacing: 0
          Rectangle{
            id: axial_mouse
            Layout.fillWidth: true
            height:20
            color:"#282832"
            Text{
              text: "     Axial"
              color: "white"
              font.family: "Helvetica"
              font.pointSize: 12
              font.weight: Font.Black
              height: parent.height
              verticalAlignment : Text.AlignVCenter
            }

            MouseArea{
              acceptedButtons: Qt.AllButtons
              anchors.fill: parent
              property real change_size: vtk_axial.width

              onDoubleClicked:{
                if(vtk_axial.width <= change_size){
                  sagital_layout.visible = false
                  coronal_layout.visible = false
                  volume_layout.visible = false
                  axial_layout.Layout.maximumHeight = -1
                  change_size = vtk_coronal.width
                }else{
                  axial_layout.Layout.maximumHeight = 480
                  sagital_layout.visible = true
                  coronal_layout.visible = true
                  volume_layout.visible = true
                }
              }
            }
          }
          VtkImage {
            id: vtk_axial
            name: "vtk_axial"
            source: "image://" + name
            objectName: name
            Layout.fillHeight: true
            Layout.fillWidth: true
          }
        }
        ColumnLayout{
          id: volume_layout
          spacing: 0
          Rectangle{
            id: volume_mouse
            Layout.fillWidth: true
            height:20
            color:"#282832"
            Text{
              text: "     Volume"
              color: "white"
              font.family: "Helvetica"
              font.pointSize: 12
              font.weight: Font.Black
              height: parent.height
              verticalAlignment : Text.AlignVCenter
            }

            MouseArea{
              acceptedButtons: Qt.AllButtons
              anchors.fill: parent
              property real change_size: vtk_volume.width

              onDoubleClicked:{
                if(vtk_volume.width <= change_size){
                  sagital_layout.visible = false
                  axial_layout.visible = false
                  coronal_layout.visible = false
                  volume_layout.Layout.maximumHeight = -1
                  change_size = vtk_coronal.width
                  //volume_mouse.implicitWidth = right_layout.width
                }else{
                  volume_layout.Layout.maximumHeight = 480
                  sagital_layout.visible = true
                  axial_layout.visible = true
                  coronal_layout.visible = true
                  //volume_mouse.implicitWidth = right_layout.width/2
                }
              }
            }
          }
          VtkImage {
            id: vtk_volume
            name: "vtk_volume"
            source: "image://" + name
            objectName: name
            Layout.fillHeight: true
            Layout.fillWidth: true
          }
        }
      }
    }

    Tab {
      title: "<b>Other Tab</b>"

      GridLayout {
        id: grid
        anchors.fill: parent
        columns: 2
        rows: 2

        Image {
          source: "source/3D.png"
          Layout.fillHeight: true
          Layout.fillWidth: true
        }
        Image {
          source: "source/cross_sectional.png"
          Layout.fillHeight: true
          Layout.fillWidth: true
        }
        Image {
          source: "source/axial.png"
          Layout.fillHeight: true
          Layout.fillWidth: true
        }
        Image {
          source: "source/panorama.png"
          Layout.fillHeight: true
          Layout.fillWidth: true
        }
      }
    }

    Tab {title: "<b>Other Tab</b>"}
    Tab {title: "<b>Other Tab</b>"}
    Tab {title: "<b>Other Tab</b>"}
    Tab {title: "<b>Other Tab</b>"}
    Tab {title: "<b>Other Tab</b>"}
    Tab {title: "<b>Other Tab</b>"}
    Tab {title: "<b>Other Tab</b>"}
    Tab {title: "<b>Other Tab</b>"}
    Tab {title: "<b>Other Tab</b>"}
    Tab {title: "<b>Other Tab</b>"}
    Tab {title: "<b>Other Tab</b>"}
    Tab {title: "<b>Other Tab</b>"}
    Tab {title: "<b>Other Tab</b>"}

    style: TabViewStyle {
      frameOverlap: 1
      tab: Rectangle {
        color: styleData.selected ? "#262626" :"#404040"
        border.color:  "#17375E"
        implicitWidth: Math.max(text.width + 4, 120)
        implicitHeight: 30
        radius: 2
        Text {
            id: text
            anchors.centerIn: parent
            text: styleData.title
            color: styleData.selected ? "#0B9ED3" : "#355A87"
        }
      }
    }
  }
}
