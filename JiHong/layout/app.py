from PyQt5.QtWidgets import QApplication
from PyQt5.QtCore import QUrl, QObject
from PyQt5.QtQuick import QQuickView
from vtk_on_qtquick_ver1 import Reader, Volume, Slice, cy_vtk
#from pprint import pprint
import sys, os, json

#sys.path.append('C:/Users/cybermed/Desktop/python/pyqt_study/layout/vtk_on_qtquick_ver1')
#print(sys.path)

if __name__ == '__main__':
    app = QApplication(sys.argv)
    reader = Reader.Reader('C:/sample_Dicom/')
    volume = Volume.Volume(reader)
    coronal = Slice.Slice(reader, 'coronal')
    sagital = Slice.Slice(reader, 'sagital')
    axial = Slice.Slice(reader, 'axial')
    view = cy_vtk.vtk_on_qtquick()
    view.addVolume('vtk_volume', volume)
    view.addVolume('vtk_coronal', coronal)
    view.addVolume('vtk_sagital', sagital)
    view.addVolume('vtk_axial', axial)

    with open('E:/SourceTree/JiHong/layout/json/data.json', 'r', -1, 'utf-8') as json_file:
        data = json.load(json_file)
    #pprint(data[0]["province"])

    view.setSource(QUrl.fromLocalFile(os.path.join(os.path.dirname(__file__), 'layout.qml')))
    view.initializeSigSlot()
    view.setResizeMode(QQuickView.SizeRootObjectToView)

    view2 = QQuickView()
    view2.setSource(QUrl.fromLocalFile(os.path.join(os.path.dirname(__file__), 'modules/ImplantPlacement.qml')))
    item = view2.rootObject().findChild(QObject, 'implant_placement')
    item.json_sample(data)

    view.show()

    sys.exit(app.exec_())






