import QtQuick 2.0
import QtGraphicalEffects 1.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.0

Rectangle{
  id: rect2
  width: parent.width
  height: parent.height/5
  y: parent.height - parent.height/3
  border.color: "#404040"
  border.width : 3
  radius: 2
  color: "#262626"

  RowLayout{
    id: row_layout1
    spacing : 0
    anchors.horizontalCenter: parent.horizontalCenter
    anchors.top: parent.top
    anchors.topMargin: 5
    Text{
      text: "<b><i>General Tools</i></b>"
      font.pointSize: 10
      color: "#00B0F0"
    }
  }

  RowLayout{
    id: row_layout2
    anchors.horizontalCenter: row_layout1.horizontalCenter
    anchors.top: row_layout1.bottom
    anchors.topMargin: 15
    spacing : 0
    ColumnLayout{
      Row{
        id:button_row1
        spacing: Qt.platform.os === "windows" ? 0 : 4
        Button { width: 30; height: 30 }
        Button { width: 30; height: 30 }
        Button { width: 30; height: 30 }
        Button { width: 30; height: 30 }
        Button { width: 30; height: 30 }
        Button { width: 30; height: 30 }
        Button { width: 30; height: 30 }
      }
      Row{
        anchors.horizontalCenter: button_row1.horizontalCenter
        spacing: Qt.platform.os === "windows" ? 0 : 4
        Button { width: 30; height: 30 }
        Button { width: 30; height: 30 }
        Button { width: 30; height: 30 }
      }
    }
  }

  RowLayout{
    id: row_layout3
    anchors.horizontalCenter: row_layout2.horizontalCenter
    anchors.top: row_layout2.bottom
    //anchors.topMargin: 5
    //spacing: 15

    ColumnLayout{
      id: fontsize_column
      anchors.right: fontcolor_column.left
      anchors.rightMargin: 10
      Text{
        id:fontsize_text
        text:"Font Size"
        color: "white"
        anchors.centerIn: parent
      }
      ComboBox {
        id:fontsize_combo
        implicitWidth: 85
        anchors.top : fontsize_text.bottom
        anchors.topMargin: 5
        anchors.horizontalCenter: fontsize_text.horizontalCenter
        model: [ "10pt", "15pt", "20pt" ]
      }
    }
    ColumnLayout{
      id: fontcolor_column
      Text{
        id:fontcolor_text
        text:"Font Color"
        color: "white"
        anchors.centerIn: parent
      }
      Rectangle{
        property color change_color: color
        id:fontcolor_rect2
        color:"#FF0000"
        width: 85
        height: 20
        radius: 10
        anchors.top : fontcolor_text.bottom
        anchors.topMargin: 5
        anchors.horizontalCenter: fontcolor_text.horizontalCenter

        MouseArea{
          anchors.fill: parent
          onClicked:{
            colorDialog.visible = true
          }
        }
      }
      ColorDialog {
        id: colorDialog
        onAccepted: {
          fontcolor_rect2.color = colorDialog.color
          Qt.quit()
        }
        onRejected: { Qt.quit() }
      }
    }
  }
}
