import QtQuick 2.7
import QtGraphicalEffects 1.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2

Item{
  width: parent.width
  height: parent.height

  Rectangle{
    id: rect3
    //width: parent.width
    //height: parent.height - rect2.height
    //y: rect2.height
    anchors.fill: parent
    border.color: "#404040"
    border.width : 3
    radius: 2
    color: "#262626"



    RowLayout{
      id: imp_row_layout1
      spacing : 0
      anchors.horizontalCenter: parent.horizontalCenter
      anchors.top: parent.top
      anchors.topMargin: 5
      Text{
        text: "<b><i>Implant Placement</i></b>"
        font.pointSize: 10
        color: "#00B0F0"
      }
    }

    RowLayout{
      id: imp_row_layout2
      anchors.horizontalCenter: parent.horizontalCenter
      anchors.top: imp_row_layout1.bottom
      anchors.topMargin: 10
      Row{
        spacing: Qt.platform.os === "windows" ? 0 : 4
        Button { width: 30; height: 30 }
        Button { width: 30; height: 30 }
        Button { width: 30; height: 30 }
        Button { width: 30; height: 30 }
        Button { width: 30; height: 30 }
        Button { width: 30; height: 30 }
        Button { width: 30; height: 30 }
        Button { width: 30; height: 30 }
        Button { width: 30; height: 30 }
      }
    }

    RowLayout{
      id: imp_row_layout3
      anchors.horizontalCenter: imp_row_layout2.horizontalCenter
      anchors.top: imp_row_layout2.bottom
      anchors.topMargin: 10

      GridLayout{
        id: imp_grid_layout1
        columns: 2
        rows: 2
        Button { implicitWidth: 100; implicitHeight: 30; text:"Arch";}
        Button { implicitWidth: 100; implicitHeight: 30; text:"Nerve Tracing";}
        Label {
          text: "  Nerve Radius:"
          font.pixelSize: 13
          font.italic: true
          color: "white"
        }
        ComboBox{ implicitWidth: 100; implicitHeight: 20; model:["1.0mm", "2.0mm", "3.0mm"] }
      }
    }

    RowLayout{
      id: imp_row_layout4
      anchors.horizontalCenter: imp_row_layout3.horizontalCenter
      anchors.top: imp_row_layout3.bottom
      anchors.topMargin: 10
      GridLayout{
        id: imp_grid_layout2
        columns: 2
        rows: 2
        Button {
          implicitWidth: 100
          implicitHeight: 30
          text:"Implant Chart"
          onClicked: {
            implant_chart_Dialog.open()
          }
        }
        Button { implicitWidth: 100; implicitHeight: 30; text:"Verification";}
        Button { implicitWidth: 100; implicitHeight: 30; text:"Anchor Pin";}
        Button { implicitWidth: 100; implicitHeight: 30; text:"Surgical Kit";}
      }
    }

    RowLayout{
      id: imp_row_layout5
      anchors.horizontalCenter: imp_row_layout4.horizontalCenter
      anchors.top: imp_row_layout4.bottom
      anchors.topMargin: 10

      Button { implicitHeight: 30; text:"Density Analysis";}
    }

    ColumnLayout{
      id: imp_Column_layout6
      anchors.horizontalCenter: imp_row_layout5.horizontalCenter
      anchors.top: imp_row_layout5.bottom
      anchors.topMargin: 10
      Row{
        spacing: 10
        Column{
          Label {
            id: label1
            text: "Implant Safety Cylinder"
            font.pixelSize: 13
            font.italic: true
            color: "white"
          }
          Row{
            anchors.horizontalCenter: parent.horizontalCenter
            topPadding: 10
            anchors.topMargin: 5
            spacing: 3
            Label {
              height: 30
              text: "On"
              font.pixelSize: 13
              font.italic: true
              color: "white"
              //verticalAlignment : Label.AlignVCenter
            }
            Switch {
              id: implant_safety_cylinder
              checked: false
              onCheckedChanged: {
                if(implant_safety_cylinder.checked == false)
                  console.log("Select On")
                else
                  console.log("Select Off")
              }
              style: SwitchStyle {
                groove: Rectangle {
                  implicitWidth: 60
                  implicitHeight: 20
                  color: control.activeFocus ? "#ffff1a" : "#ffff1a"
                  border.width: 1
                }
              }
            }
            Label {
              text: "Off"
              font.pixelSize: 13
              font.italic: true
              color: "white"
            }
          }
        }
        Column{
          Label {
            text: "Tooth Numbering"
            font.pixelSize: 13
            font.italic: true
            color: "white"
          }

          Row{
            anchors.horizontalCenter: parent.horizontalCenter
            topPadding: 10
            anchors.topMargin: 5
            spacing: 3
            Label {
              text: "FDI"
              font.pixelSize: 13
              font.italic: true
              color: "white"
            }
            Switch {
              id: tooth_tumbering
              checked: false
              onCheckedChanged: {
                if(tooth_tumbering.checked == false)
                  console.log("Select FDI")
                else
                  console.log("Select Universal")
              }
              style: SwitchStyle {
                groove: Rectangle {
                  implicitWidth: 60
                  implicitHeight: 20
                  color: control.activeFocus ? "#ffff1a" : "#ffff1a"
                  border.width: 1
                }
              }
            }
            Label {
              text: "Universal"
              font.pixelSize: 13
              font.italic: true
              color: "white"
            }
          }
        }
      }
    }

    Dialog{
      id: implant_chart_Dialog

      contentItem: Rectangle{
        implicitWidth: 800
        implicitHeight: 600
        anchors.fill: parent

        GroupBox {
          title: "Select Item"
          width: 300
          height: 300

          ListModel{
            id: city_model

            objectName: "implant_placement"

            property var arr_data: []

            function json_sample(data){
              var dict_data
              /*for(var i = 0; i < data.length; i++){
              }*/
              dict_data = {"name" : "aa"}
              console.log(dict_data[0].value)
              console.log(data.length)
            }
            /*
            ListElement{
              name: 서울특별시
            }
            ListElement{
              name: "대전광역시"
            }
            ListElement{
              name: "부산광역시"
            }
            */

            Component.onCompleted: {
              append({"name": "A"})
              append({"name": "B"})
              append({"name": "C"})
              city_model.append({"name": "D"})
            }

            /*
            Component.onCompleted:{
              for(var i = 0; i < data.length; i++){
                city_model.arr_data.push(data[i]["province"])
              }
              console.log(city_model.arr_data)
            }
            */
          }

          ListView{
            id: list
            anchors.fill: parent
            model: city_model
            delegate: Component{
              Item{
                id:cityDelegate
                width: parent.width
                height: 40
                Column{
                  spacing:10
                  Text{ text: "city : " + name }
                }
                MouseArea {
                  anchors.fill: parent
                  onClicked: list.currentIndex = index
                }
              }
            }
            highlight: Rectangle {
              color: 'grey'
              opacity: 0.3
              Text {
                anchors.centerIn: parent
                text: city_model.get(list.currentIndex).name
                color: 'black'
              }
            }
            onCurrentItemChanged: console.log(city_model.get(list.currentIndex).name + ' selected')
          }
        }
      }
    }
  }
}