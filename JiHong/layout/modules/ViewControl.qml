import QtQuick 2.7
import QtGraphicalEffects 1.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.3

Rectangle{
  id: rect1
  width: parent.width
  height: parent.height / 2.5
  border.color: "#404040"
  border.width : 3
  radius: 2
  color: "#262626"

  RowLayout{
    id: view_control_row_layout1
    spacing : 0
    anchors.horizontalCenter: parent.horizontalCenter
    anchors.top: parent.top
    anchors.topMargin: 5
    Text{
      text: "<b><i>View Control</i></b>"
      font.pointSize: 10
      color: "#00B0F0"
    }
  }

  RowLayout{
    id: view_control_row_layout2
    anchors.horizontalCenter: parent.horizontalCenter
    anchors.top: view_control_row_layout1.bottom
    anchors.topMargin: 10
    Row{
      spacing: Qt.platform.os === "windows" ? 0 : 4
      Button {
        width: 30
        height: 30
        Image{
          anchors.fill: parent
          source: "../source/1.jpg"
        }
        onClicked: console.log("click")
      }
      Button { width: 30; height: 30 }
      Button { width: 30; height: 30 }
      Button { width: 30; height: 30 }
      Button { width: 30; height: 30 }
      Button { width: 30; height: 30 }
      Button { width: 30; height: 30 }
      Button { width: 30; height: 30 }
      Button { width: 30; height: 30 }
    }
  }

  RowLayout{
    id: view_control_row_layout3
    anchors.horizontalCenter: parent.horizontalCenter
    anchors.top: view_control_row_layout2.bottom
    anchors.topMargin: 10
    spacing : 0

    ComboBox {
      implicitWidth: 100
      implicitHeight: 25
      anchors.verticalCenter : parent.verticalCenter
      model: [ "Greyscale", "Blackscale", "Whitescale" ]
    }

    Row{
      leftPadding: 5
      rightPadding: 5
      spacing: 3
      Text{
        id: text_2D
        height: 30
        text:"2D"
        color: "white"
        verticalAlignment : Text.AlignVCenter
      }
      Switch {
        id: change_2D_3D
        height: 30
        checked: false
        onCheckedChanged: {
          if(change_2D_3D.checked == false)
            console.log("Select 2D")
          else
            console.log("Select 3D")
        }
        style: SwitchStyle {
          groove: Rectangle {
            implicitWidth: 60
            implicitHeight: 20
            color: control.activeFocus ? "#ffff1a" : "#ffff1a"
            border.width: 1
          }
        }
      }
      Text{
        id: text_3D
        height: 30
        text:"3D"
        color: "white"
        verticalAlignment : Text.AlignVCenter
      }
    }

    Button { implicitWidth: 30; implicitHeight: 30 }
    Button { implicitWidth: 30; implicitHeight: 30 }
  }

  RowLayout{
    id: view_control_row_layout4
    anchors.horizontalCenter: parent.horizontalCenter
    anchors.top: view_control_row_layout3.bottom
    anchors.topMargin: 10
    spacing : 0

    Column{
      spacing: 2
      Label {
        text: "Brightness"
        font.pixelSize: 13
        color: "white"
        anchors.horizontalCenter: parent.horizontalCenter
      }
      Slider {
        activeFocusOnPress: true
        minimumValue: 1.0
        maximumValue: 100.0
        anchors.horizontalCenter: parent.horizontalCenter
      }
      Label {
        text: "Contrast"
        font.pixelSize: 13
        color: "white"
        anchors.horizontalCenter: parent.horizontalCenter
      }
      Slider {
        activeFocusOnPress: true
        minimumValue: 1.0
        maximumValue: 100.0
        anchors.horizontalCenter: parent.horizontalCenter
      }
      Label {
        text: "Opacity"
        font.pixelSize: 13
        color: "white"
        anchors.horizontalCenter: parent.horizontalCenter
      }
      Slider {
        activeFocusOnPress: true
        minimumValue: 1.0
        maximumValue: 100.0
        anchors.horizontalCenter: parent.horizontalCenter
      }
    }
  }

  RowLayout{
    id: view_control_row_layout5
    anchors.horizontalCenter: parent.horizontalCenter
    anchors.top: view_control_row_layout4.bottom
    anchors.topMargin: 10
    spacing : 0

    GridLayout{
      columns: 2
      rows: 2
      Button { implicitWidth: 100; implicitHeight: 30; text:"Bone" }
      Button { implicitWidth: 100; implicitHeight: 30; text:"Bone && Tissue" }
      Button { implicitWidth: 100; implicitHeight: 30; text:"More" }
      Button { implicitWidth: 100; implicitHeight: 30; text:"Custom" }
    }
  }
  RowLayout{
    id: view_control_row_layout6
    anchors.horizontalCenter: parent.horizontalCenter
    anchors.top: view_control_row_layout5.bottom
    anchors.topMargin: 10
    spacing : 4

    Column{
      spacing: 2
      Label {
        text: "Filter"
        font.pixelSize: 13
        color: "white"
        anchors.horizontalCenter: parent.horizontalCenter
      }
      ComboBox {
        anchors.horizontalCenter : parent.horizontalCenter
        width: 100
        model: [ "Sharpen x1", "Sharpen x2", "Sharpen x3" ]
      }
    }
    Column{
      spacing: 2
      Label {
        text: "Slice Thickness"
        font.pixelSize: 13
        color: "white"
        anchors.horizontalCenter: parent.horizontalCenter
      }
      ComboBox {
        anchors.horizontalCenter : parent.horizontalCenter
        width: 100
        model: [ "0.5 mm", "1.0 mm", "1.5 mm" ]
      }
    }
  }
}