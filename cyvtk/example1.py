from qapp import _qapp
from cyvtk import cy_vtk
import vtk


class Volume(cy_vtk.Vtk_image_holder):

    def __init__(self, *args, **kwds):
        super().__init__()

        # source
        self.reader = vtk.vtkDICOMImageReader()
        self.reader.SetDirectoryName(r'/Users/scott/Dicom/1')
        self.reader.Update()

        self.c_f, self.o_f = vtk.vtkColorTransferFunction(), vtk.vtkPiecewiseFunction()
        self.p = vtk.vtkVolumeProperty()
        self.m, self.v = vtk.vtkSmartVolumeMapper(), vtk.vtkVolume()

        opacityWindow = 4096
        opacityLevel = 2048

        # mapper
        self.m.SetInputConnection(self.reader.GetOutputPort())
        self.m.SetBlendModeToMaximumIntensity()

        # property
        self.p.SetIndependentComponents(True)
        self.c_f.AddRGBSegment(0.0, 1.0, 1.0, 1.0, 255.0, 1.0, 1.0, 1.0)
        self.o_f.AddSegment(opacityLevel - 0.5*opacityWindow, 0.0, opacityLevel + 0.5*opacityWindow, 1.0)
        self.p.SetColor(self.c_f)
        self.p.SetScalarOpacity(self.o_f)
        self.p.SetInterpolationTypeToLinear()

        # actor
        self.v.SetMapper(self.m)
        self.v.SetProperty(self.p)

        self.ren.AddViewProp(self.v)

        # camera
        camera = self.ren.GetActiveCamera()
        c = self.v.GetCenter()
        camera.SetFocalPoint(c)
        camera.SetPosition(c[0], c[1]+400, c[2])
        camera.SetViewUp(0, 0, -1)

    def mousePressEvent(self, e):
        print('Mouse Pressed')
        super().mousePressEvent(e)

    def mouseReleaseEvent(self, e):
        print('Mouse Released')
        super().mouseReleaseEvent(e)

    def mouseMoveEvent(self, e):
        print('Mouse Moved')
        super().mouseMoveEvent(e)


if __name__ == '__main__':
    win = Volume()
    win.show()

    _qapp.exec_()
