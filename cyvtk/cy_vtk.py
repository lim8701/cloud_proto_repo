from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPainter, QImage
from PyQt5.QtWidgets import QWidget, QSizePolicy

import vtk
from vtk.qt.QVTKRenderWindowInteractor import QVTKRenderWindowInteractor

from qapp import _qapp

# TODO!!!!!!!!!!!
import platform

_IS_MAC = (platform.system() == 'Darwin')


class Vtk_image_holder(QWidget):
    """
    __image_dirty: indicator stating image needs to be captured
    """

    def __init__(self, parent=None, wcont=None, istyle=None, *args, **kwds):
        super().__init__(parent, *args, **kwds)

        self.wcont = wcont

        self.setMouseTracking(True)  # get all mouse events
        self.setSizePolicy(QSizePolicy(*([QSizePolicy.Expanding] * 2)))
        self.setAttribute(Qt.WA_OpaquePaintEvent)  # prevent Qt's background fill

        self.view = QVTKRenderWindowInteractor()
        self.img = QImage()
        self.__last_image_num, self.__image_num = 0, 0

        self.istyle = istyle or vtk.vtkInteractorStyleTrackballCamera()
        self.view._Iren.SetInteractorStyle(self.istyle)

        self.view.Start()

        self.ren = vtk.vtkRenderer()
        self.view._RenderWindow.AddRenderer(self.ren)
        self.view._RenderWindow.AddObserver('EndEvent', self.__on_image_rendered)

    def closeEvent(self, e):
        super().closeEvent(e)
        self.view.closeEvent(e)

    def finalize(self):
        self.view.Finalize()

    def sizeHint(self):
        return self.view.sizeHint()

    def __on_image_rendered(self, _o, _e):
        # print('    1. rendered!!!')
        self.__image_num += 1

        if self.wcont:
            raise NotImplementedError
            self.update()
            # self.wcont.repaint()
        else:
            # self.update()
            self.repaint()

    def paintEvent(self, _e):
        # print('    2. paint ', end='')
        if self.__last_image_num < self.__image_num:
            # print('+ capture ', end='')
            w, h = self.view._RenderWindow.GetSize()

            if self.img.width() != w or self.img.height() != h:
                self.img = QImage(w, h, QImage.Format_RGB32)

            b = self.img.bits()  # sip.voidptr object
            b.setsize(self.img.byteCount())  # enabling Python buffer protocol

            va = vtk.vtkUnsignedCharArray()
            # va.SetVoidArray(b, self.img.byteCount(), 1)
            va.SetVoidArray(b, w * h * 4, 1)
            self.view._RenderWindow.GetRGBACharPixelData(0, 0, w - 1, h - 1, 1, va)

            self.img = self.img.rgbSwapped()

            self.__last_image_num = self.__image_num

        # print(_e.type(), _e.rect())

        p = QPainter(self)

        # Do transformation. This makes img.mirrored() avoided.
        p.scale(1, -1)
        p.translate(0, -self.height())

        p.drawImage(0, 0, self.img)

    def resizeEvent(self, e):
        # NOTE Even though we request view.resize() here, view.resizeEvent()
        #   will not be triggered, because view is not shown. Hence, we do
        #   necessary things here.
        # print('  resize', e.size())
        w, h = self.width(), self.height()

        self.view.resize(w, h)
        self.view._RenderWindow.SetSize(w, h)
        self.view._Iren.SetSize(w, h)
        self.view._Iren.ConfigureEvent()

        if _IS_MAC:
            # NOTE As of Python 3.5 and PyQt 5.7, without this, scene is not
            #   transformed appropriately during resize on Mac.
            # TODO? Due to the below, rendering is done twice during resize.
            self.view.show()
            self.view.hide()

        self.view._RenderWindow.Render()

        # print('        ', e.size(), 'resize')

    def enterEvent(self, e):
        self.view.enterEvent(e)

    def leaveEvent(self, e):
        self.view.leaveEvent(e)

    def mouseMoveEvent(self, e):
        # if self.view._ActiveButton != Qt.NoButton:
        #    #print('  mmove', (e.x(), e.y()))

        self.view.mouseMoveEvent(e)

        # if self.view._ActiveButton != Qt.NoButton:
        #    #print('       ', (e.x(), e.y()), 'mmove')

    def mousePressEvent(self, e):
        # print('  mpress', (e.x(), e.y()))

        self.view.mousePressEvent(e)

        # print('       ', (e.x(), e.y()), 'mpress')

    def mouseReleaseEvent(self, e):
        # print('  mrelease', (e.x(), e.y()))

        self.view.mouseReleaseEvent(e)

        # print('       ', (e.x(), e.y()), 'mrelease')
        # self.view._ActiveButton = Qt.NoButton

    def keyPressEvent(self, e):
        self.view.keyPressEvent(e)

    def keyReleaseEvent(self, e):
        self.view.keyReleaseEvent(e)

    def wheelEvent(self, e):
        d = e.angleDelta().y()

        self.istyle.SetMouseWheelMotionFactor(abs(d) / 120)

        if d == 0:
            return
        elif d > 0:
            self.view._Iren.MouseWheelForwardEvent()
        else:
            self.view._Iren.MouseWheelBackwardEvent()


#
# Install vtk message hook.

def hook_vtk_message():
    """
    Duplicate vtk message to qCritical/qWarning and stdout.

    NOTE At least up to vtk 6.3, it is almost impossible to hide 'vtk output
      window.' This seems best.

    NOTE Sometimes this is called when vtk is trying to paint, which can lead
      to 'recursive repaint' in view of Qt, which results in Qt warning message
      that users get additionally. (TODO?)
    """
    from PyQt5.QtCore import qCritical, qWarning
    def display_error(obj, evt):
        qCritical('vtk %s: Check vtk output window for detail.' % evt)

    def display_warning(obj, evt):
        qWarning('vtk %s: Check vtk output window for detail.' % evt)

    o = vtk.vtkOutputWindow.GetInstance()
    o.AddObserver(vtk.vtkCommand.ErrorEvent, display_error)
    o.AddObserver(vtk.vtkCommand.WarningEvent, display_warning)
    # Duplicate message to stderr (Windows only).
    try:
        o.SendToStdErrOn()
    except:
        pass


hook_vtk_message()


#
# A few example pipelines

def actor_cone():
    s, m, a = vtk.vtkConeSource(), vtk.vtkPolyDataMapper(), vtk.vtkActor()
    m.SetInputConnection(s.GetOutputPort())
    a.SetMapper(m)
    return a


def actor_balls():
    ss, gl = vtk.vtkSphereSource(), vtk.vtkGlyph3D()
    B, Bm, Ba = vtk.vtkAppendPolyData(), vtk.vtkPolyDataMapper(), vtk.vtkActor()
    gl.SetInputConnection(ss.GetOutputPort())
    gl.SetSourceConnection(ss.GetOutputPort())
    gl.SetVectorModeToUseNormal()
    gl.SetScaleModeToScaleByVector()
    gl.SetScaleFactor(0.15)
    B.AddInputConnection(ss.GetOutputPort())
    B.AddInputConnection(gl.GetOutputPort())
    Bm.SetInputConnection(B.GetOutputPort())
    Ba.SetMapper(Bm)
    return Ba


if __name__ == '__main__':
    win = Vtk_image_holder()
    win.ren.AddViewProp(actor_cone())
    win.show()

    _qapp.exec_()
