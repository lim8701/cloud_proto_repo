from PyQt5.QtGui import QGuiApplication
from PyQt5.QtCore import QObject, QUrl
from PyQt5.QtQuick import QQuickView
import sys, os


app = QGuiApplication(sys.argv)

view = QQuickView()
view.setSource(QUrl.fromLocalFile(os.path.join(os.path.dirname(__file__), 'app.qml')))
view.setResizeMode(QQuickView.SizeRootObjectToView)
view.show()

'''
First!!!
load histogram data
'''
f = open('../histogram_with_qquick/I.dat', 'r')
H = eval(f.readline())
minX = 0
maxX = len(H)
lenX = maxX - minX
offsetX = minX
minY = int(min(H))
maxY = int(max(H))

item = view.rootObject().findChild(QObject, 'histogram_item')
item.set_datas(H, minX, maxX, minY, maxY)

app.exec_()
