import QtQuick 2.0
import "../histogram_with_qquick"
import "../windowing_with_qquick"
import "../colormap_with_qquick"

Column {
    width: 410
    height: 390
    spacing: 10

    Histogram {
        width: parent.width-10
        height: 200
        x: 5
    }

    Windowing {
        width: parent.width
        height: 30
    }

    Colormap {
        width: parent.width
        height: 140
    }
}