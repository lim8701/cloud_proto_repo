import QtQuick 2.0

Item {
    id: main
    width: 300
    height: 200

    Canvas {
        id: mycanvas
        width: parent.width
        height: parent.height

        property var arrX: []
        property var arrY: []

        onPaint: {
            var ctx = getContext("2d");
            ctx.resetTransform();
            ctx.fillStyle = 'black';
            ctx.fillRect(0, 0, width, height);

            ctx.lineCap = "square";
            ctx.lineWidth = 1;
            ctx.strokeStyle = Qt.rgba(1, 1, 1, 1);

            for(var i=1; i<arrX.length; i++)
            {
                if((arrX[i-1] == -1 || arrY[i-1] == -1) || ((arrX[i] == -1 || arrY[i] == -1)))
                    continue;
                ctx.beginPath();
                ctx.moveTo(arrX[i-1], arrY[i-1]);
                ctx.lineTo(arrX[i], arrY[i]);
                ctx.stroke();
            }
        }

        MouseArea {
            acceptedButtons: Qt.AllButtons
            anchors.fill: parent

            onReleased : {
                parent.arrX.push(-1)
                parent.arrY.push(-1)
            }
            onPositionChanged : {
                parent.arrX.push(mouse.x)
                parent.arrY.push(mouse.y)
                parent.requestPaint()
            }
        }
    }
}