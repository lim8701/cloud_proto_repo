from PyQt5.QtGui import QGuiApplication
from PyQt5.QtCore import QUrl
from PyQt5.QtQuick import QQuickView
import sys, os


app = QGuiApplication(sys.argv)

view = QQuickView()
view.setSource(QUrl.fromLocalFile(os.path.join(os.path.dirname(__file__), 'paint_with_qquick.qml')))
view.setResizeMode(QQuickView.SizeRootObjectToView)
view.show()

app.exec_()
