from PyQt5.QtGui import QGuiApplication
from PyQt5.QtCore import QObject, QUrl
from PyQt5.QtQuick import QQuickView
from random import randrange
import sys, os


app = QGuiApplication(sys.argv)

view = QQuickView()
view.setSource(QUrl.fromLocalFile(os.path.join(os.path.dirname(__file__), 'Histogram.qml')))
view.setResizeMode(QQuickView.SizeRootObjectToView)
view.show()

'''
randomly generate intensity values
then, accumulate histogram
'''
# I = [randrange(100) for _ in range(10000)]
# minX = min(I)
# maxX = max(I)
# offsetX = minX
# lenX = maxX - minX
# H = [0] * (lenX+1)
# for i in I:
#     index = int(i)-offsetX
#     H[index] = H[index] + 1
# minY = int(min(H))
# maxY = int(max(H))

'''
load histogram data
'''
f = open('I.dat', 'r')
H = eval(f.readline())
minX = 0
maxX = len(H)
lenX = maxX - minX
offsetX = minX
minY = int(min(H))
maxY = int(max(H))

item = view.rootObject().findChild(QObject, 'histogram_item')
item.set_datas(H, minX, maxX, minY, maxY)

app.exec_()
