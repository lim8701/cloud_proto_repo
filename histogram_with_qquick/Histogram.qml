import QtQuick 2.0

Column {
    id: histogram
    width: 400
    height: 200

    Item {
        objectName: "histogram_item"
        width: parent.width
        height: parent.height
        property var arr_values: []
        property real minX: 0
        property real maxX: 0
        property real minY: 0
        property real maxY: 0
        property real scaleY: 1
        property real scale_min: 0.2
        property real scale_max: 8

        function set_datas(_vals, _minX, _maxX, _minY, _maxY)
        {
            for(var i=0; i<_vals.length; i++)
            {
                arr_values.push(_vals[i])
            }

            minX = _minX
            maxX = _maxX
            minY = _minY
            maxY = _maxY
        }

        Canvas {
            id: mycanvas
            width: parent.width
            height: parent.height
            antialiasing: true

            property real scaleY : parent.scaleY
            Behavior on scaleY {SpringAnimation { spring: 2; damping: 0.2; loops:Animation.Infinite }}
            //Behavior on scaleY {NumberAnimation { easing.type: Easing.OutElastic; easing.amplitude: 3.0; easing.period: 2.0; duration: 300 }}
            onScaleYChanged:requestPaint();

            onPaint: {
                var ctx = getContext("2d");
                ctx.resetTransform();
                ctx.fillStyle = 'white';
                ctx.fillRect(0, 0, width, height);
                ctx.strokeStyle = Qt.rgba(0, 0, 0, 1);

                var aspect_x = (parent.maxX - parent.minX + 1) / width;
                var aspect_y = parent.maxY / height / mycanvas.scaleY;

                for(var i=1; i<parent.arr_values.length; i++)
                {
                    var x1 = (i-1) / aspect_x;
                    var x2 = i / aspect_x;
                    var y1 = height - (parent.arr_values[i-1] / aspect_y);
                    var y2 = height - (parent.arr_values[i] / aspect_y);

                    ctx.beginPath();
                    ctx.moveTo(x1, y1);
                    ctx.lineTo(x2, y2);
                    ctx.stroke();
                }
            }
        }

        MouseArea {
            width: parent.width
            height: parent.height

            onWheel: {
                var delta = wheel.angleDelta.y
                if(delta > 0 && parent.scaleY <= parent.scale_max)
                    parent.scaleY += 0.1
                else if(delta < 0 && parent.scaleY >= parent.scale_min)
                    parent.scaleY -= 0.1
            }
        }
    }
}