import QtQuick 2.0


Row {
    width: 1600
    height: 400
    property real img_width: width/4
    property real img_height: height
    spacing: 1

    VtkImage {
        name: "vtk_volume"
        source: "image://" + name
        objectName: name
        width: img_width
        height: img_height
    }

    VtkImage {
        name: "vtk_coronal"
        source: "image://" + name
        objectName: name
        width: img_width
        height: img_height
    }

    VtkImage {
        name: "vtk_sagital"
        source: "image://" + name
        objectName: name
        width: img_width
        height: img_height
    }

    VtkImage {
        name: "vtk_axial"
        source: "image://" + name
        objectName: name
        width: img_width
        height: img_height
    }
}
