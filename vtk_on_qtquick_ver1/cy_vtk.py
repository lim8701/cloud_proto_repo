import platform
from random import random

import vtk
from PyQt5.QtCore import QObject
from PyQt5.QtCore import QSize, Qt, QEvent, QPointF
from PyQt5.QtCore import pyqtSignal, pyqtSlot
from PyQt5.QtGui import QImage, QColor
from PyQt5.QtGui import QMouseEvent, QWheelEvent
from PyQt5.QtQuick import QQuickImageProvider, QQuickView
from vtk.qt.QVTKRenderWindowInteractor import QVTKRenderWindowInteractor

_IS_MAC = (platform.system() == 'Darwin')


class Vtk_image_holder(QObject):
    """
    __image_dirty: indicator stating image needs to be captured
    """

    sigRefresh = pyqtSignal(object)
    sigResize = pyqtSignal(int, int)

    def __init__(self, parent=None, istyle=None, *args, **kwds):
        super().__init__(parent, *args, **kwds)

        self.view = QVTKRenderWindowInteractor()
        self.img = QImage()
        self.__last_image_num, self.__image_num = 0, 0

        self.istyle = istyle or vtk.vtkInteractorStyleTrackballCamera()
        self.view._Iren.SetInteractorStyle(self.istyle)

        self.ren = vtk.vtkRenderer()
        self.view._RenderWindow.AddRenderer(self.ren)
        self.view._RenderWindow.AddObserver('EndEvent', self.__on_image_rendered)

        self.sigResize.connect(self.resize, Qt.QueuedConnection)

    def finalize(self):
        self.view.Finalize()

    def __on_image_rendered(self, _o, _e):
        # print('    1. rendered!!!')
        self.__image_num += 1

        # print('    2. paint ', end='')
        if self.__last_image_num < self.__image_num:
            # print('+ capture ', end='')
            w, h = self.view._RenderWindow.GetSize()

            if self.img.width() != w or self.img.height() != h:
                self.img = QImage(w, h, QImage.Format_RGB32)

            b = self.img.bits()  # sip.voidptr object
            b.setsize(self.img.byteCount())  # enabling Python buffer protocol

            va = vtk.vtkUnsignedCharArray()
            # va.SetVoidArray(b, self.img.byteCount(), 1)
            va.SetVoidArray(b, w * h * 4, 1)
            self.view._RenderWindow.GetRGBACharPixelData(0, 0, w - 1, h - 1, 1, va)

            self.img = self.img.rgbSwapped()
            self.img = self.img.mirrored(False, True)

            self.__last_image_num = self.__image_num

        # print(_e.type(), _e.rect())

        self.sigRefresh.emit(self.img)

    def resize(self, w, h):
        # NOTE Even though we request view.resize() here, view.resizeEvent()
        #   will not be triggered, because view is not shown. Hence, we do
        #   necessary things here.
        # print('  resize', e.size())

        self.view.resize(w, h)
        self.view._RenderWindow.SetSize(w, h)
        self.view._Iren.SetSize(w, h)
        self.view._Iren.ConfigureEvent()

        if _IS_MAC:
            # NOTE As of Python 3.5 and PyQt 5.7, without this, scene is not
            #   transformed appropriately during resize on Mac.
            # TODO? Due to the below, rendering is done twice during resize.
            self.view.show()
            self.view.hide()

        self.view._RenderWindow.Render()

        # print('        ', e.size(), 'resize')

    def enterEvent(self, e):
        self.view.enterEvent(e)

    def leaveEvent(self, e):
        self.view.leaveEvent(e)

    def mouseMoveEvent(self, e):
        # if self.view._ActiveButton != Qt.NoButton:
        #    #print('  mmove', (e.x(), e.y()))

        self.view.mouseMoveEvent(e)

        # if self.view._ActiveButton != Qt.NoButton:
        #    #print('       ', (e.x(), e.y()), 'mmove')

    def mousePressEvent(self, e):
        # print('  mpress', (e.x(), e.y()))

        self.view.mousePressEvent(e)

        # print('       ', (e.x(), e.y()), 'mpress')

    def mouseReleaseEvent(self, e):
        # print('  mrelease', (e.x(), e.y()))

        self.view.mouseReleaseEvent(e)

        # print('       ', (e.x(), e.y()), 'mrelease')
        # self.view._ActiveButton = Qt.NoButton

    def keyPressEvent(self, e):
        self.view.keyPressEvent(e)

    def keyReleaseEvent(self, e):
        self.view.keyReleaseEvent(e)

    def wheelEvent(self, e):
        self.wheelEventByAngleDelta(e.angleDelta())

    def wheelEventByAngleDelta(self, delta):
        d = delta.y()

        self.istyle.SetMouseWheelMotionFactor(abs(d) / 120)

        if d > 0:
            self.view._Iren.MouseWheelForwardEvent()
        elif d < 0:
            self.view._Iren.MouseWheelBackwardEvent()


class vtk_on_qtquick(QQuickView):
    """ VTK On QtQuick """

    class MyQQuickImageProvider(QQuickImageProvider):
        """ Image Provider """

        def __init__(self, *args, **kwds):
            super().__init__(QQuickImageProvider.Image, *args, **kwds)
            self.vtk_img = None
            self.size = None

        def requestImage(self, id, size):
            if self.vtk_img is None:
                img = QImage(100, 100, QImage.Format_RGB32)
                img.fill(QColor(0, 0, 0))
                return img, QSize(100, 100)

            print('requestImag!!!!!', self.size)

            return self.vtk_img, self.size

    def __init__(self, *args, **kwds):
        super().__init__(*args, **kwds)

        self.V = dict()
        self.IP = dict()

        self.context = self.rootContext()
        self.engine = self.context.engine()
        self.closing.connect(self.closeEvent)

    def initializeSigSlot(self):

        # re-defined
        _enumMouseButtons = {0: Qt.NoButton, 134217727: Qt.AllButtons, 1: Qt.LeftButton, 2: Qt.RightButton,
                             4: Qt.MidButton, 8: Qt.BackButton, 16: Qt.ForwardButton, 32: Qt.TaskButton}
        _enumModifier = {0: Qt.NoModifier, 33554432: Qt.ShiftModifier, 67108864: Qt.ControlModifier,
                         134217728: Qt.AltModifier, 268435456: Qt.MetaModifier, 536870912: Qt.KeypadModifier,
                         1073741824: Qt.GroupSwitchModifier}

        @pyqtSlot(object, object)
        def _mouseMoveEvent(_id, _m):
            m = _m.toVariant()
            mouseEvent = QMouseEvent(QEvent.MouseMove, QPointF(m['x'], m['y']), m['button'],
                                     _enumMouseButtons[m['buttons']], _enumModifier[m['modifiers']])
            self.V[_id].mouseMoveEvent(mouseEvent)

        @pyqtSlot(object, object)
        def _mousePressEvent(_id, _m):
            m = _m.toVariant()
            mouseEvent = QMouseEvent(QEvent.MouseButtonPress, QPointF(m['x'], m['y']), m['button'],
                                     _enumMouseButtons[m['buttons']], _enumModifier[m['modifiers']])
            self.V[_id].mousePressEvent(mouseEvent)

        @pyqtSlot(object, object)
        def _mouseReleaseEvent(_id, _m):
            m = _m.toVariant()
            mouseEvent = QMouseEvent(QEvent.MouseButtonRelease, QPointF(m['x'], m['y']), m['button'],
                                     _enumMouseButtons[m['buttons']], _enumModifier[m['modifiers']])
            self.V[_id].mouseReleaseEvent(mouseEvent)

        @pyqtSlot(object, object)
        def _wheelEvent(_id, _w):
            w = _w.toVariant()
            self.V[_id].wheelEventByAngleDelta(w['angleDelta'])

        @pyqtSlot(object, object, object)
        def _resizeEvent(_id, _w, _h):
            # self.V[_id].resize(int(_w), int(_h))
            self.V[_id].sigResize.emit(int(_w), int(_h))

        for n in self.V:
            i = self.rootObject().findChild(QObject, n)
            i.sigMousePressed.connect(_mousePressEvent, Qt.QueuedConnection)
            i.sigMouseReleased.connect(_mouseReleaseEvent, Qt.QueuedConnection)
            i.sigMouseMoved.connect(_mouseMoveEvent, Qt.QueuedConnection)
            i.sigWheel.connect(_wheelEvent)
            i.sigResize.connect(_resizeEvent)
            i.adjustSize()

    def addVolume(self, id, volume):
        image_provider = self.MyQQuickImageProvider()
        self.V[id] = volume
        self.IP[id] = image_provider
        self.engine.addImageProvider(id, image_provider)

        @pyqtSlot(object)
        def _refresh(_img):
            self.refreshImage(id, _img)

        volume.sigRefresh.connect(_refresh, Qt.QueuedConnection)
        volume.view._RenderWindow.Render()

    def closeEvent(self, e):
        for i in self.V:
            print('closed : ', self.V[i])
            self.V[i].finalize()

    def refreshImage(self, _id, _img):
        # TODO is this sufficient ???
        # I don't know what is this proper usage...

        if self.rootObject() is None:
            return

        for n in self.V:
            i = self.rootObject().findChild(QObject, n)
            # source name check
            img_name = i.property('source').url()
            first_index = img_name.rfind('//') + 2
            last_index = None if img_name.rfind('__') == -1 else img_name.rfind('__') - 1
            img_name = img_name[first_index:last_index]
            if _id == img_name:
                assert self.IP[img_name] and self.V[img_name]
                self.IP[img_name].vtk_img = _img
                self.IP[img_name].size = QSize(self.V[img_name].img.width(), self.V[img_name].img.height())
                i.setProperty('source', 'image://%s/__%f' % (img_name, random()))


#
# Install vtk message hook.

def hook_vtk_message():
    """
    Duplicate vtk message to qCritical/qWarning and stdout.

    NOTE At least up to vtk 6.3, it is almost impossible to hide 'vtk output
      window.' This seems best.

    NOTE Sometimes this is called when vtk is trying to paint, which can lead
      to 'recursive repaint' in view of Qt, which results in Qt warning message
      that users get additionally. (TODO?)
    """
    from PyQt5.QtCore import qCritical, qWarning
    def display_error(obj, evt):
        qCritical('vtk %s: Check vtk output window for detail.' % evt)

    def display_warning(obj, evt):
        qWarning('vtk %s: Check vtk output window for detail.' % evt)

    o = vtk.vtkOutputWindow.GetInstance()
    o.AddObserver(vtk.vtkCommand.ErrorEvent, display_error)
    o.AddObserver(vtk.vtkCommand.WarningEvent, display_warning)
    # Duplicate message to stderr (Windows only).
    try:
        o.SendToStdErrOn()
    except:
        pass


hook_vtk_message()
