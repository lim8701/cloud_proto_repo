from PyQt5 import QtCore
import vtk
import SimpleITK


class Reader(QtCore.QObject):

    def __init__(self, dcm, *args, **kwds):
        super().__init__(*args, **kwds)

        assert dcm
        img = self.read(dcm)
        self.vtk_image = self.convert_to_vtkimage(img)

    def get_vtkimage(self):
        return self.vtk_image

    def read(self, str_path):
        # Read by ITK
        if str_path:
            if r'.vtk' not in str_path:
                reader = SimpleITK.ImageSeriesReader()
                str_path = reader.GetGDCMSeriesFileNames(str_path)
                reader.SetFileNames(str_path)

            if len(str_path) == 0:
                return None

            img = SimpleITK.ReadImage(str_path)

            return img

    def convert_to_vtkimage(self, sitk_img=None):
        if sitk_img is None:
            return None

        data = SimpleITK.GetArrayFromImage(sitk_img)

        img_spacing = sitk_img.GetSpacing()
        img_cen = sitk_img.GetOrigin()

        img_data = data.astype('int16')
        img_string = img_data.tostring()
        dim = data.shape

        importer = vtk.vtkImageImport()
        importer.CopyImportVoidPointer(img_string, len(img_string))
        importer.SetNumberOfScalarComponents(1)

        extent = importer.GetDataExtent()
        importer.SetDataExtent(extent[0], extent[0] + dim[2] - 1,
                               extent[2], extent[2] + dim[1] - 1,
                               extent[4], extent[4] + dim[0] - 1)
        importer.SetWholeExtent(extent[0], extent[0] + dim[2] - 1,
                                extent[2], extent[2] + dim[1] - 1,
                                extent[4], extent[4] + dim[0] - 1)

        importer.SetDataSpacing(img_spacing[0], img_spacing[1], img_spacing[2])
        importer.SetDataOrigin(img_cen)

        # have to call 'Update'!!!
        importer.Update()

        return importer

    def get_range_of_volume(self):
        """
        :return: xMin, xMax, yMin, yMax, zMin, zMax
        """
        return self.vtk_image.GetExecutive().GetWholeExtent(self.vtk_image.GetOutputInformation(0))

    def get_spacing_of_volume(self):
        """
        :return: xSpacing, ySpacing, zSpacing
        """
        return self.vtk_image.GetOutput().GetSpacing()

    def get_origin_of_volume(self):
        """
        :return: OriginX, OriginY, OriginZ
        """
        return self.vtk_image.GetOutput().GetOrigin()
