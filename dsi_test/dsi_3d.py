from PyQt5 import QtCore, QtWidgets, Qt
from PyQt5.QtWidgets import *

import vtk
from vtk.qt.QVTKRenderWindowInteractor import QVTKRenderWindowInteractor
from vtk.util import numpy_support
import numpy as np
import math

import cyCafe
import _qapp


def create_sphere_actor(pos, radius=0.2, color=[1, 0, 0]):
    s = vtk.vtkSphereSource()
    s.SetRadius(radius)
    s.SetCenter(pos)
    s.Update()
    m = vtk.vtkPolyDataMapper()
    m.SetInputData(s.GetOutput())
    a = vtk.vtkActor()
    a.GetProperty().SetColor(color)
    a.SetMapper(m)
    return a

def create_line_actor(pts, radius=0.2, color=[0,0,1]):
    points = vtk.vtkPoints()
    polyline = vtk.vtkPolyLine()
    cells = vtk.vtkCellArray()
    polyData = vtk.vtkPolyData()
    tube = vtk.vtkTubeFilter()

    for p in pts:
        points.InsertNextPoint(p)
    polyline.GetPointIds().SetNumberOfIds(len(pts))
    for i in range(len(pts)):
        polyline.GetPointIds().SetId(i, i)
    cells.InsertNextCell(polyline)
    polyData.SetPoints(points)
    polyData.SetLines(cells)

    tube.SetInputData(polyData)
    tube.SetNumberOfSides(36)
    tube.SetRadius(radius)
    tube.SetCapping(True)
    tube.Update()

    # Transform & Transform Filter
    transform = vtk.vtkTransform()
    tpd_filter = vtk.vtkTransformPolyDataFilter()
    tpd_filter.SetInputData(tube.GetOutput())
    tpd_filter.SetTransform(transform)
    tpd_filter.Update()

    mapper_line = vtk.vtkPolyDataMapper()
    mapper_line.SetInputData(tpd_filter.GetOutput())

    actor_line = vtk.vtkActor()
    actor_line.SetMapper(mapper_line)
    actor_line.GetProperty().SetColor(color)
    return actor_line

def get_vtkmat_from_list(list_mat, transpose=False):
    matrix = vtk.vtkMatrix4x4()
    for i, v in enumerate(list_mat):
        r = i // 4
        c = i - r * 4
        matrix.SetElement(r, c, v)

    if transpose:
        matrix.Transpose()

    return matrix


class DSI_AXIS(QtCore.QObject):

    sig_update = QtCore.pyqtSignal(object, object)

    def __init__(self, parent, axis, color=[1, 0, 0], mode=0, _ren=None, *args, **kwds):
        super().__init__()

        self.mode = mode    # 0 : arrow is move // 1 : arrow is scale

        self._ren = _ren    # for test

        self.parent = parent
        self.obj_actor, self.obj_tpd = parent.obj_info
        self.bound = self.obj_actor.GetBounds() if self.obj_actor else [0, 1, 0, 1, 0, 1]
        self.center = [(self.bound[0]+self.bound[1])/2, (self.bound[2]+self.bound[3])/2, (self.bound[4]+self.bound[5])/2]

        # NOTE!! compute polydata's center which is derived from "source"!!!
        # it's different from actor's center
        _pd = self.obj_tpd.GetInput()
        _pd.ComputeBounds()
        _bnd = _pd.GetBounds()
        self.obj_center = [(_bnd[0]+_bnd[1])/2, (_bnd[2]+_bnd[3])/2, (_bnd[4]+_bnd[5])/2]

        # extent
        b = self._ren.ComputeVisiblePropBounds()
        _max = max([b[1] - b[0],
                    b[3] - b[2],
                    b[5] - b[4]])
        # _max = max([self.bound[1] - self.bound[0],
        #             self.bound[3] - self.bound[2],
        #             self.bound[5] - self.bound[4]])
        _offset = abs(_max * 0.2)
        self.bound = [self.bound[0]-_offset, self.bound[1]+_offset,
                      self.bound[2]-_offset, self.bound[3]+_offset,
                      self.bound[4]-_offset, self.bound[5]+_offset]

        self.axis = axis
        _pt0 = list(self.center)
        _pt1 = list(self.center)
        _pt0[axis] = self.bound[axis * 2]
        _pt1[axis] = self.bound[axis * 2 + 1]

        # self.item_scale = _max * 0.005
        self.item_scale = _max * 0.003

        vec = np.subtract(_pt1, _pt0)
        if self.axis == 0:
            hor_vec = [0, 1, 0]
        elif self.axis == 1:
            hor_vec = [0, 0, 1]
        elif self.axis == 2:
            hor_vec = [1, 0, 0]
        self.line_param = {'pt': [_pt0, _pt1], 'vec': vec, 'hor_vec': hor_vec}    # Points, Vec
        self.color = color
        self.actors = []
        self.actors_test = []
        self.init_controller()

    def init_controller(self):
        self.init_axis_line()
        self.init_control_handle(0)
        self.init_control_handle(1)
        self.init_arrow(0)
        self.init_arrow(1)

        self.init_circle()

    def init_axis_line(self):
        self.points = vtk.vtkPoints()
        self.polyline = vtk.vtkPolyLine()
        self.cells = vtk.vtkCellArray()
        self.polyData = vtk.vtkPolyData()
        self.tube = vtk.vtkTubeFilter()

        offset = self.line_param['vec'] * 0.05
        P = [self.line_param['pt'][0] - offset, self.line_param['pt'][1] + offset]
        for p in P:
            self.points.InsertNextPoint(p.tolist())
        self.polyline.GetPointIds().SetNumberOfIds(len(self.line_param['pt']))
        for i in range(len(self.line_param['pt'])):
            self.polyline.GetPointIds().SetId(i, i)
        self.cells.InsertNextCell(self.polyline)
        self.polyData.SetPoints(self.points)
        self.polyData.SetLines(self.cells)

        self.tube.SetInputData(self.polyData)
        self.tube.SetNumberOfSides(36)
        self.tube.SetRadius(self.item_scale)
        self.tube.SetCapping(True)
        self.tube.Update()

        # Transform & Transform Filter
        self.transform = vtk.vtkTransform()
        self.tpd_filter = vtk.vtkTransformPolyDataFilter()
        self.tpd_filter.SetInputData(self.tube.GetOutput())
        self.tpd_filter.SetTransform(self.transform)
        self.tpd_filter.Update()

        self.mapper_line = vtk.vtkPolyDataMapper()
        self.mapper_line.SetInputData(self.tpd_filter.GetOutput())

        self.actor_line = vtk.vtkActor()
        self.actor_line.SetMapper(self.mapper_line)
        self.actor_line.GetProperty().SetColor(self.color)
        self.actor_line.SetPickable(False)
        self.actor_line.GetProperty().SetOpacity(0.15)

        self.actors.append(self.actor_line)

    def init_control_handle(self, idx=0):

        args = [self.item_scale*5, self.line_param['pt'][0], [.2, .7, .4]] if idx == 0 \
            else [self.item_scale*5, self.line_param['pt'][1], [.2, .7, .8]]

        # moving controller
        c = vtk.vtkSphereSource()
        c.SetRadius(args[0])
        c.SetCenter(args[1])
        c.Update()
        setattr(self, 'src_c' + str(idx), c)

        mapper_c = vtk.vtkPolyDataMapper()
        mapper_c.SetInputData(c.GetOutput())
        setattr(self, 'mapper_c' + str(idx), mapper_c)

        actor_c = vtk.vtkActor()
        actor_c.SetMapper(mapper_c)
        actor_c.GetProperty().SetColor(args[2])

        setattr(self, 'actor_c' + str(idx), actor_c)
        self.actors.append(actor_c)

    def init_arrow(self, idx=0):

        arrow_info = [self.actor_c0, -1, 3.14] if idx == 0 else [self.actor_c1, 1, 0]

        # translate to actor c center + offset
        _x, _y, _z = arrow_info[0].GetCenter()
        _offset = self.line_param['vec'] * 0.07 * arrow_info[1]
        _mat1 = get_vtkmat_from_list([1, 0, 0, _x + _offset[0],
                                      0, 1, 0, _y + _offset[1],
                                      0, 0, 1, _z + _offset[2],
                                      0, 0, 0, 1])

        # angle between vectors
        _rot_curve = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
        _angle = (vtk.vtkMath.AngleBetweenVectors(self.line_param['vec'], [1, 0, 0]) * -1) + arrow_info[2]

        _eps = np.finfo(float).eps
        _up = [0, 1, 0] if np.inner(self.line_param['vec'], [1, 0, 0]) >= (1-_eps) \
            else np.cross(self.line_param['vec'], [1, 0, 0])
        _up = _up / np.linalg.norm(_up)

        # NOTE!!! Quaternion Q : w, x, y, z  =>  cos(theta/2), v*sin(theta/2)
        _v = np.multiply(_up, math.sin(_angle / 2))
        vtk.vtkMath.QuaternionToMatrix3x3([math.cos(_angle / 2), _v[0], _v[1], _v[2]], _rot_curve)
        _mat2 = get_vtkmat_from_list([_rot_curve[0][0], _rot_curve[0][1], _rot_curve[0][2], 0,
                                      _rot_curve[1][0], _rot_curve[1][1], _rot_curve[1][2], 0,
                                      _rot_curve[2][0], _rot_curve[2][1], _rot_curve[2][2], 0,
                                      0, 0, 0, 1])

        # scale
        _mat3 = get_vtkmat_from_list([1 * self.item_scale * 20, 0, 0, 0,
                                      0, 1, 0, 0,
                                      0, 0, 1, 0,
                                      0, 0, 0, 1])

        _mat = vtk.vtkMatrix4x4()
        vtk.vtkMatrix4x4.Multiply4x4(_mat2, _mat3, _mat)

        s = vtk.vtkArrowSource()
        s.SetShaftResolution(36)
        # s.SetShaftRadius(self.item_scale*1.25)
        s.SetShaftRadius(self.item_scale*3)
        s.SetTipResolution(36)
        s.SetTipLength(0.5)
        # s.SetTipRadius(self.item_scale*3.5)
        s.SetTipRadius(self.item_scale*7)
        s.Update()
        t = vtk.vtkTransform()
        t.Concatenate(_mat)
        tpd = vtk.vtkTransformPolyDataFilter()
        tpd.SetTransform(t)
        tpd.SetInputConnection(s.GetOutputPort())
        m = vtk.vtkPolyDataMapper()
        m.SetInputConnection(tpd.GetOutputPort())
        a = vtk.vtkActor()
        a.SetMapper(m)
        a.GetProperty().SetColor([1, 0, 0.5])
        a.SetUserMatrix(_mat1)

        setattr(self, 'src_arrow' + str(idx), s)
        setattr(self, 'tpd_arrow' + str(idx), tpd)
        setattr(self, 'mapper_arrow' + str(idx), m)
        setattr(self, 'actor_arrow' + str(idx), a)
        self.actors.append(a)

    def init_circle(self):
        points = vtk.vtkPoints()
        # polyline = vtk.vtkPolyLine()
        # cells = vtk.vtkCellArray()
        polyData = vtk.vtkPolyData()
        tube = vtk.vtkTubeFilter()

        # generate a set of circle points and then, generate polydata
        self.generate_circle_points(center=self.center, num=360, out_points=points)
        self.generate_circle_polydata(src_points=points, ref_polydata=polyData)

        tube.SetInputData(polyData)
        tube.SetNumberOfSides(36)
        tube.SetRadius(self.item_scale * 2)
        tube.SetCapping(True)
        tube.Update()

        m_circle = vtk.vtkPolyDataMapper()
        m_circle.SetInputData(tube.GetOutput())
        a_circle = vtk.vtkActor()
        a_circle.SetMapper(m_circle)
        self.actors.append(a_circle)

        a_circle.GetProperty().SetOpacity(0.1)
        a_circle.SetPickable(False)

        setattr(self, 'polydata_circle', polyData)
        setattr(self, 'tube_circle', tube)
        setattr(self, 'mapper_circle', m_circle)
        setattr(self, 'actor_circle', a_circle)

    def generate_circle_points(self, radius=None, center=None, num=360, out_points=None):
        assert type(out_points) is vtk.vtkPoints, "check out_points."

        # generate a set of circle points
        center = self.center if center is None else center
        norm = np.linalg.norm(self.line_param['vec'])
        radius = norm / 2 if radius is None else radius

        # create circle on [0, 0, 1] and rotate it by axis
        rot_curve = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
        angle = 3.14 / 2   # 90 degree

        if self.axis == 0:
            rot_axis = [0, 0, 1]
        elif self.axis == 1:
            rot_axis = [0, 1, 0]
        elif self.axis == 2:
            rot_axis = [1, 0, 0]

        # NOTE!!! Quaternion Q : w, x, y, z  =>  cos(theta/2), v*sin(theta/2)
        v = np.multiply(rot_axis, math.sin(angle / 2))
        vtk.vtkMath.QuaternionToMatrix3x3([math.cos(angle / 2), v[0], v[1], v[2]], rot_curve)

        for i in range(num):
            angle = 2 * np.pi * i / num
            pt = [np.cos(angle) * radius, np.sin(angle) * radius, 0]
            pt = np.matmul(rot_curve, pt) + center
            out_points.InsertNextPoint(pt)

    def generate_circle_polydata(self, src_points, ref_polydata):
        _cells = vtk.vtkCellArray()
        _polyline = vtk.vtkPolyLine()
        num = src_points.GetNumberOfPoints()

        _polyline.GetPointIds().SetNumberOfIds(num)
        for i in range(num):
            _polyline.GetPointIds().SetId(i, i)
        _cells.InsertNextCell(_polyline)
        ref_polydata.SetPoints(src_points)
        ref_polydata.SetLines(_cells)

    def update_point(self, pts, mov_info=None, rot_info=None, _emit=True):

        _ori = np.linalg.norm(np.subtract(self.line_param['pt'][1], self.line_param['pt'][0]))
        _new = np.linalg.norm(np.subtract(pts[1], pts[0]))
        self.ratio = float(_new / _ori)

        # update line
        self.points.Reset()
        self.line_param['pt'] = pts
        self.line_param['vec'] = np.subtract(pts[1], pts[0])

        offset = self.line_param['vec'] * 0.05
        P = [pts[0] - offset, pts[1] + offset]
        for p in P:
            self.points.InsertNextPoint(p.tolist())

        self.polyData.Modified()
        self.tube.Update()
        self.tpd_filter.Update()
        self.mapper_line.Update()
        self.actor_line.Modified()

        # update control_handle
        if hasattr(self, 'src_c0') and hasattr(self, 'src_c1'):
            self.src_c0.SetCenter(pts[0])
            self.src_c0.Update()
            self.mapper_c0.Update()
            self.actor_c0.Modified()
            self.src_c1.SetCenter(pts[1])
            self.src_c1.Update()
            self.mapper_c1.Update()
            self.actor_c1.Modified()

        # update arrows
        if hasattr(self, 'src_arrow0') and hasattr(self, 'src_arrow1'):
            def _update_matrix(__t, _a, __mat):
                if rot_info:
                    angle, rot_axis, rot_mat, cen_mat, plane_vec = rot_info

                    # Rotation
                    __m = vtk.vtkMatrix4x4()
                    __m.DeepCopy(__t.GetMatrix())
                    __m2 = get_vtkmat_from_list(np.array(rot_mat).reshape(16).tolist())
                    __m3 = vtk.vtkMatrix4x4()
                    vtk.vtkMatrix4x4.Multiply4x4(__m2, __m, __m3)
                    __t.SetMatrix(__m3)
                    __t.Update()

                # Translation
                _t_mat = [[1, 0, 0, __mat[0]],
                          [0, 1, 0, __mat[1]],
                          [0, 0, 1, __mat[2]],
                          [0, 0, 0, 1]]
                __n = get_vtkmat_from_list(np.array(_t_mat).reshape(16).tolist())
                _a.SetUserMatrix(__n)
                _a.Modified()

            # arrow0
            _offset = self.line_param['vec'] * 0.07 * -1
            _new_arrow0_pos = np.add(self.actor_c0.GetCenter(), _offset)
            __t = self.tpd_arrow0.GetTransform()
            _a = self.actor_arrow0
            _update_matrix(__t, _a, _new_arrow0_pos)
            self.tpd_arrow0.Update()
            self.mapper_arrow0.Update()
            self.actor_arrow0.Modified()

            # arrow1
            _offset = self.line_param['vec'] * 0.07
            _new_arrow1_pos = np.add(self.actor_c1.GetCenter(), _offset)
            __t = self.tpd_arrow1.GetTransform()
            _a = self.actor_arrow1
            _update_matrix(__t, _a, _new_arrow1_pos)
            self.tpd_arrow1.Update()
            self.mapper_arrow1.Update()
            self.actor_arrow1.Modified()

        # update circle
        if hasattr(self, 'actor_circle'):
            _pts = self.polydata_circle.GetPoints()

            # Rotate
            if rot_info:
                angle, rot_axis, rot_mat, cen_mat, plane_vec = rot_info
                _P = numpy_support.vtk_to_numpy(_pts.GetData())
                _pts.Reset()

                test_mode = 1
                if test_mode == 0:
                    # python for loop (not recommend)
                    _P2 = []
                    for _p in _P:
                        pt = self.rotate_point(_p, rot_mat, cen_mat)
                        _P2.append(pt)
                        _pts.InsertNextPoint(pt[0:3])
                else:
                    # numpy (recommend!!)
                    _P = self.rotate_points(_P, rot_mat, cen_mat)
                    for _p in _P:
                        _pts.InsertNextPoint(_p)

                _pts.Modified()
            else:
                _cen = np.eye(4, 3)

                # Move
                if mov_info:
                    displacement, _ = mov_info
                    _cen_mat = np.concatenate((_cen, np.append(np.add(self.center, displacement[0:3]), [1]).reshape(4, -1)), axis=1)
                # Scale
                else:
                    _cen_mat = np.concatenate((_cen, np.append(self.center, [1]).reshape(4, -1)), axis=1)

                _cen_mat_rev = np.concatenate((_cen, np.append(np.multiply(self.center, -1), [1]).reshape(4, -1)), axis=1)
                _scale_mat = np.eye(3, 3) * self.ratio
                _scale_mat = np.concatenate((_scale_mat, np.array([0] * 3).reshape(1, 3)), axis=0)
                _scale_mat = np.concatenate((_scale_mat, np.array([0, 0, 0, 1]).reshape(4, 1)), axis=1)

                _mat = np.matmul(_cen_mat, _scale_mat)
                _mat = np.matmul(_mat, _cen_mat_rev)

                _P = numpy_support.vtk_to_numpy(_pts.GetData())
                _pts.Reset()
                _append_mat = np.array([1] * len(_P))
                _P = np.concatenate((_P, _append_mat.reshape(len(_P), -1)), axis=1)
                _P = np.matmul(_mat, _P.transpose())
                _P = np.delete(_P, 3, 0).transpose()
                for _p in _P:
                    _pts.InsertNextPoint(_p[0:3])
                _pts.Modified()

            self.polydata_circle.Modified()
            self.tube_circle.Update()
            self.mapper_circle.Update()
            self.actor_circle.Modified()

        # sig emit
        if _emit:
            self.sig_update.emit(mov_info, rot_info)

    def rotate_point(self, pt, rot_mat, cen_mat):
        cen_mat_rev = [[*m[0:3], -m[3]] for m in cen_mat[0:3]] + [cen_mat[3]]
        new_pt = pt
        new_pt = np.matmul(np.array(cen_mat_rev), new_pt.tolist()+[1])
        new_pt = np.matmul(rot_mat, new_pt)
        new_pt = np.matmul(cen_mat, new_pt)
        return new_pt

    def rotate_points(self, pts, rot_mat, cen_mat):
        cen_mat_rev = [[*m[0:3], -m[3]] for m in cen_mat[0:3]] + [cen_mat[3]]

        _append_mat = np.array([1]*len(pts))
        new_pts = np.concatenate((pts, _append_mat.reshape(len(pts), -1)), axis=1)
        new_pts = new_pts.transpose()

        _mat = np.matmul(cen_mat, rot_mat)
        _mat = np.matmul(_mat, cen_mat_rev)
        _new_pts = np.matmul(_mat, new_pts)
        _new_pts = np.delete(_new_pts, 3, 0).transpose()
        return _new_pts

    def update(self, move_info, rot_info, _sender):
        """
        move_info : displacemnet, reserved1
        rot_info : angle, rot_axis, rot_mat, cen_mat, plane_vec
        """
        if (move_info is None and rot_info is None) or _sender is None:
            return
        if _sender is self:
            return

        if move_info:
            displacement, _ = move_info
            self.move_from_update_event(displacement)
            pass

        if rot_info:
            angle, rot_axis, rot_mat, cen_mat, plane_vec = rot_info
            self.rotate_from_update_event(angle, rot_axis, rot_mat, cen_mat, plane_vec, _sender=_sender)

    def update_ratio(self):
        # TODO!!!!!!!!!!!!
        pass

    def get_actors(self):
        return self.actors

    def test(self, ren):
        self.ren_for_test = ren

    def interact(self, _actor, _pos, _prev_pos, _cam):

        def _get_intersect_infos():
            """
            return : intersect point, plane A, plane B, line C
            """
            # line intersection of two planes
            vec_a = self.line_param['vec']
            up = _cam.GetViewUp()
            _v1 = _cam.GetPosition()
            _v2 = _cam.GetFocalPoint()
            view = np.subtract(_v2, _v1)
            hor = np.cross(up, view / np.linalg.norm(view))
            vec_b = np.array(hor if abs(np.dot(vec_a / np.linalg.norm(vec_a), up)) > 0.6 else up)
            vec_c = np.cross(vec_a, vec_b)  # plane1
            vec_c = vec_c / np.linalg.norm(vec_c)

            vec_d = np.cross(vec_b, view)  # plane2
            vec_d = vec_d / np.linalg.norm(vec_d)

            # equation
            d1 = (-1 * vec_c[0] * self.line_param['pt'][0][0]) + (-1 * vec_c[1] * self.line_param['pt'][0][1]) + (
                -1 * vec_c[2] * self.line_param['pt'][0][2])
            d2 = (-1 * vec_d[0] * _pos[0]) + (-1 * vec_d[1] * _pos[1]) + (-1 * vec_d[2] * _pos[2])
            a = np.array([vec_c, vec_d])
            b = np.array([-1 * d1, -1 * d2])

            # solve
            i = np.linalg.pinv(a)
            b = np.matmul(i, b)

            vec_line = vec_b
            pt0 = (vec_line * -1) + b
            pt1 = (vec_line * 1) + b
            vec1 = np.subtract(pt1, pt0)
            vec1 = vec1 / np.linalg.norm(vec1)
            vec2 = np.array(self.line_param['vec']) / np.linalg.norm(self.line_param['vec'])
            # param T, S
            _aa = np.array([[vec1[0], vec2[0] * -1], [vec1[1], vec2[1] * -1], [vec1[2], vec2[2] * -1]])
            _bb = np.array([[self.line_param['pt'][0][0] - pt0[0]],
                            [self.line_param['pt'][0][1] - pt0[1]],
                            [self.line_param['pt'][0][2] - pt0[2]]])
            pi_aa = np.linalg.pinv(_aa)
            val = np.matmul(pi_aa, _bb)
            _x = float((vec1[0] * val[0]) + pt0[0])
            _y = float((vec1[1] * val[0]) + pt0[1])
            _z = float((vec1[2] * val[0]) + pt0[2])
            return [_x, _y, _z], vec_c, vec_d, self.line_param['vec']

        def  _get_rotate_infos(infos, test=False):
            """
            return : angle(radian), rotation matrix, center matrix, plane_vec
            """
            src = infos['src']
            sign = infos['sign']

            # plane on axis which is cross(vec, hor_vec)
            _vec = self.line_param['vec'] / np.linalg.norm(self.line_param['vec'])
            _plane_vec = np.cross(_vec, self.line_param['hor_vec'])
            _plane_pos = self.center

            # line which is lied on view_vec
            _cam_f = _cam.GetFocalPoint()
            _cam_p = _cam.GetPosition()
            _line_vec = np.subtract(_cam_f, _cam_p)
            _line_pos = list(_pos[0:3])

            # calc intersection point line and plane
            # equation
            d_tmp1 = np.inner(_plane_vec, _plane_pos) * -1
            d_tmp2 = np.inner(_plane_vec, _line_pos)
            ai_bj_ck = np.inner(_plane_vec, _line_vec)
            d = d_tmp1 + d_tmp2

            val = -1 * d / ai_bj_ck

            _X = _line_vec[0] * val + _line_pos[0]
            _Y = _line_vec[1] * val + _line_pos[1]
            _Z = _line_vec[2] * val + _line_pos[2]

            # Test Actor
            if test:
                _aaa = create_sphere_actor([_X, _Y, _Z], 1)
                self._ren.AddActor(_aaa)
                self.actors_test.append(_aaa)

            # Get Angle
            _pt0 = self.center
            _pt1 = src[0]

            _vec1 = np.subtract(_pt1, _pt0)
            _pt2 = [_X, _Y, _Z]
            _vec2 = np.subtract(_pt2, _pt0)
            _vec2 = _vec2 / np.linalg.norm(_vec2)
            angle = vtk.vtkMath.AngleBetweenVectors(_vec1, _vec2)
            print("Angle :: ", angle)

            _rotate_axis = _plane_vec if np.inner(self.line_param['hor_vec'], _vec2) >= 0 else _plane_vec * -1
            _rotate_axis_test = _rotate_axis
            _rotate_axis *= sign[0]

            # NOTE!!! Quaternion Q : w, x, y, z  =>  cos(theta/2), v*sin(theta/2)
            _rot_mat = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
            _v = np.multiply(_rotate_axis, math.sin(angle / 2))
            vtk.vtkMath.QuaternionToMatrix3x3([math.cos(angle / 2), _v[0], _v[1], _v[2]], _rot_mat)

            _mov = [[1, 0, 0, self.center[0]],
                    [0, 1, 0, self.center[1]],
                    [0, 0, 1, self.center[2]],
                    [0, 0, 0, 1]]
            __mov = [[*m[0:3], -m[3]] for m in _mov[0:3]] + [_mov[3]]

            _rot_mat[0] = _rot_mat[0] + [0]
            _rot_mat[1] = _rot_mat[1] + [0]
            _rot_mat[2] = _rot_mat[2] + [0]
            _rot_mat = _rot_mat + [[0, 0, 0, 1]]

            if test:
                new_pt = list(src[0])
                new_pt = np.matmul(np.array(__mov), new_pt + [1])
                new_pt = np.matmul(_rot_mat, new_pt)
                new_pt = np.matmul(_mov, new_pt)

                # Test Actor
                _aaa = create_sphere_actor(new_pt[0:3], 1, [0, 0, 1])
                self._ren.AddActor(_aaa)
                self.actors_test.append(_aaa)

            return angle, _rotate_axis_test, _rot_mat, _mov, _plane_vec

        # scale if arrow actor is picked
        if _actor is self.actor_arrow0 or _actor is self.actor_arrow1:
            moved_point, plane_vec_a, plane_vec_b, line_vec_c = _get_intersect_infos()
            # move mode
            if self.mode == 0:
                self.move_by_arrow(_actor, moved_point)
            # scale mode
            elif self.mode == 1:
                self.scale(_actor, moved_point)
                self.scale_obj()

        # rotate if actor_c is picked
        if _actor is self.actor_c0 or _actor is self.actor_c1:
            order_idx = (0, 1) if _actor is self.actor_c0 \
                else ((1, 0) if _actor is self.actor_c1 else None)
            if order_idx is None:
                return
            # src = [self.src_c0, self.src_c1]
            src = [self.line_param['pt'][0][0:3], self.line_param['pt'][1][0:3]]
            src = [src[i] for i in order_idx]
            sign = [-1, 1]
            sign = [sign[i] for i in order_idx]
            infos = {'src': src, 'sign': sign}

            angle, rot_axis, rot_mat, cen_mat, plane_vec = _get_rotate_infos(infos, test=False)
            self.rotate(angle, rot_axis, rot_mat, cen_mat, plane_vec)
            self.rotate_obj(angle, rot_axis, rot_mat, cen_mat, plane_vec)

        if _actor is self.obj_actor:
            _displacement = np.subtract(_pos, _prev_pos)
            self.move(_displacement)

    def move(self, displacemnet, _emit=False):
        _pt0 = np.add(self.line_param['pt'][0], displacemnet[0:3])
        _pt1 = np.add(self.line_param['pt'][1], displacemnet[0:3])
        self.center = np.add(self.center, displacemnet[0:3])
        self.update_point([_pt0[0:3], _pt1[0:3]], mov_info=[displacemnet, None], _emit=_emit)

    def move_by_arrow(self, picked_actor, picked_point):
        order_idx = (0, 1) if picked_actor is self.actor_arrow0 \
            else ((1, 0) if picked_actor is self.actor_arrow1 else None)

        if order_idx is None:
            return

        src = [self.src_c0, self.src_c1]
        src = [src[i] for i in order_idx]
        sign = [1, -1]
        sign = [sign[i] for i in order_idx]
        # offset from sphere to arrow
        offset1 = self.line_param['vec'] * 0.07 * sign[0]
        # offset from arrow end pos to arrow center
        offset2 = self.line_param['vec'] / np.linalg.norm(self.line_param['vec']) * (0.75 / 2) * sign[0]
        offset = offset1 + offset2
        picked_point = np.add(picked_point, offset)

        _displacement = np.subtract(picked_point, src[0].GetCenter())
        self.move(_displacement, _emit=True)
        self.move_obj(_displacement)

    def move_obj(self, displacement):
        tpd = self.obj_tpd
        t = tpd.GetTransform()
        mov_mat = [[1, 0, 0, displacement[0]],
                   [0, 1, 0, displacement[1]],
                   [0, 0, 1, displacement[2]],
                   [0, 0, 0, 1]]
        _m0 = get_vtkmat_from_list(np.array(mov_mat).ravel())
        _m1 = vtk.vtkMatrix4x4()
        _m1.DeepCopy(t.GetMatrix())
        _m2 = vtk.vtkMatrix4x4()
        vtk.vtkMatrix4x4.Multiply4x4(_m0, _m1, _m2)
        t.SetMatrix(_m2)
        t.Update()
        t.Modified()
        tpd.Update()
        tpd.Modified()

    def scale(self, picked_actor, picked_point):
        order_idx = (0, 1) if picked_actor is self.actor_arrow0 \
            else ((1, 0) if picked_actor is self.actor_arrow1 else None)

        if order_idx is None:
            return

        src = [self.src_c0, self.src_c1]
        src = [src[i] for i in order_idx]
        sign = [1, -1]
        sign = [sign[i] for i in order_idx]
        # offset from sphere to arrow
        offset1 = self.line_param['vec'] * 0.07 * sign[0]
        # offset from arrow end pos to arrow center
        offset2 = self.line_param['vec'] / np.linalg.norm(self.line_param['vec']) * (0.75 / 2) * sign[0]
        offset = offset1 + offset2
        picked_point = np.add(picked_point, offset)

        _c = list(src[0].GetCenter())
        displacement = np.subtract(picked_point, _c) * -1
        pair_point = np.add(src[1].GetCenter(), displacement)

        dest_pos = [picked_point, pair_point]
        dest_pos = [dest_pos[i] for i in order_idx]

        self.update_point(dest_pos, _emit=False)

    def scale_obj(self):
        t = self.obj_tpd.GetTransform()
        tpd = self.obj_tpd
        _s = [1, 1, 1]
        _s[self.axis] = self.ratio

        _m_mov = [[1, 0, 0, self.obj_center[0]],
                  [0, 1, 0, self.obj_center[1]],
                  [0, 0, 1, self.obj_center[2]],
                  [0, 0, 0, 1]]
        _m_mov_rev = [[*m[0:3], -m[3]] for m in _m_mov[0:3]]
        _m_mov_rev += [_m_mov[3]]
        _m_mov = get_vtkmat_from_list(np.array(_m_mov).ravel())
        _m_mov_rev = get_vtkmat_from_list(np.array(_m_mov_rev).ravel())

        _m_scale = [[_s[0], 0, 0, 0],
                    [0, _s[1], 0, 0],
                    [0, 0, _s[2], 0],
                    [0, 0, 0, 1]]
        _m_scale = get_vtkmat_from_list(np.array(_m_scale).ravel())

        _m0 = vtk.vtkMatrix4x4()
        _m0.DeepCopy(t.GetMatrix())
        _m0_inv = vtk.vtkMatrix4x4()
        vtk.vtkMatrix4x4.Invert(_m0, _m0_inv)
        _m1 = vtk.vtkMatrix4x4()

        vtk.vtkMatrix4x4.Multiply4x4(_m0, _m_mov, _m1)
        vtk.vtkMatrix4x4.Multiply4x4(_m1, _m_scale, _m1)
        vtk.vtkMatrix4x4.Multiply4x4(_m1, _m_mov_rev, _m1)
        vtk.vtkMatrix4x4.Multiply4x4(_m1, _m0_inv, _m1)
        vtk.vtkMatrix4x4.Multiply4x4(_m1, _m0, _m1)

        t.SetMatrix(_m1)
        t.Update()
        t.Modified()
        tpd.Update()
        tpd.Modified()

    def rotate(self, angle, rot_axis, rot_mat, cen_mat, plane_vec, _emit=True):
        cen_mat_rev = [[*m[0:3], -m[3]] for m in cen_mat[0:3]] + [cen_mat[3]]

        def _rotate(pt):
            new_pt = list(pt)
            new_pt = np.matmul(np.array(cen_mat_rev), new_pt+[1])
            new_pt = np.matmul(rot_mat, new_pt)
            new_pt = np.matmul(cen_mat, new_pt)
            return new_pt

        # update control_handle
        if hasattr(self, 'src_c0') and hasattr(self, 'src_c1'):
            _pt0 = _rotate(self.line_param['pt'][0])
            _pt1 = _rotate(self.line_param['pt'][1])

            # NOTE!!!!!!!!
            # Should have set self.line_param !!!!
            _norm = np.linalg.norm(self.line_param['vec'])
            _new_vec = np.subtract(_pt1[0:3], _pt0[0:3])
            _new_vec_n = _new_vec / np.linalg.norm(_new_vec)
            if _emit:
                new_hor_vec = np.cross(plane_vec, _new_vec_n)
                self.line_param['hor_vec'] = new_hor_vec
            self.line_param['pt'] = [_pt0[0:3], _pt1[0:3]]
            self.line_param['vec'] = _new_vec_n * _norm
            self.update_point([_pt0[0:3], _pt1[0:3]], rot_info=[angle, rot_axis, rot_mat, cen_mat, plane_vec], _emit=_emit)

    def rotate_obj(self, angle, rot_axis, rot_mat, cen_mat, plane_vec):
        t = self.obj_tpd.GetTransform()
        tpd = self.obj_tpd

        cen_mat_rev = [[*m[0:3], -m[3]] for m in cen_mat[0:3]] + [cen_mat[3]]
        _m0 = np.matmul(cen_mat, rot_mat)
        _m0 = np.matmul(_m0, cen_mat_rev)
        _m0 = get_vtkmat_from_list(_m0.ravel())
        _m1 = vtk.vtkMatrix4x4()
        _m1.DeepCopy(t.GetMatrix())
        _m2 = vtk.vtkMatrix4x4()
        vtk.vtkMatrix4x4.Multiply4x4(_m0, _m1, _m2)
        t.SetMatrix(_m2)
        t.Update()
        tpd.Update()
        tpd.Modified()

    def move_from_update_event(self, displacement):
        self.move(displacement, _emit=False)

    def rotate_from_update_event(self, angle, rot_axis, rot_mat, cen_mat, plane_vec, _sender):
        cen_mat_rev = [[*m[0:3], -m[3]] for m in cen_mat[0:3]] + [cen_mat[3]]

        def _rotate(pt):
            new_pt = list(pt)
            new_pt = np.matmul(np.array(cen_mat_rev), new_pt+[1])
            new_pt = np.matmul(rot_mat, new_pt)
            new_pt = np.matmul(cen_mat, new_pt)
            return new_pt

        # update control_handle
        if hasattr(self, 'src_c0') and hasattr(self, 'src_c1'):

            _pt0 = _rotate(self.line_param['pt'][0])
            _pt1 = _rotate(self.line_param['pt'][1])

            # NOTE!!!!!!!!
            # Should have set self.line_param !!!!
            _norm = np.linalg.norm(self.line_param['vec'])
            _new_vec = np.subtract(_pt1[0:3], _pt0[0:3])
            _new_vec_n = _new_vec / np.linalg.norm(_new_vec)

            if _sender.axis == 0:
                if self.axis == 1:
                    self.line_param['hor_vec'] = rot_axis
                elif self.axis == 2:
                    self.line_param['hor_vec'] = _sender.line_param['vec'] / np.linalg.norm(_sender.line_param['vec'])
            elif _sender.axis == 1:
                if self.axis == 0:
                    self.line_param['hor_vec'] = _sender.line_param['vec'] / np.linalg.norm(_sender.line_param['vec'])
                elif self.axis == 2:
                    self.line_param['hor_vec'] = rot_axis
            elif _sender.axis == 2:
                if self.axis == 1:
                    self.line_param['hor_vec'] = _sender.line_param['vec'] / np.linalg.norm(_sender.line_param['vec'])
                elif self.axis == 0:
                    self.line_param['hor_vec'] = rot_axis

            self.line_param['pt'] = [_pt0[0:3], _pt1[0:3]]
            self.line_param['vec'] = _new_vec_n * _norm
            self.update_point([_pt0[0:3], _pt1[0:3]], rot_info=[angle, rot_axis, rot_mat, cen_mat, plane_vec], _emit=False)


class DSI_3D_OBJ(QtCore.QObject):

    def __init__(self, obj_info, mode=0, _iren=None, _ren=None, *args, **kwds):
        """
        :param obj_info:    0: actor, 1: transform, 2: transform polydata filter
        :param mode:        0 : arrow is move // 1 : arrow is scale
        :param _iren:
        :param _ren:
        """

        self.fn_convert_pos = kwds['_fn_convert_pos']

        super().__init__()
        self.obj_info = obj_info

        dsi_axis_x = DSI_AXIS(self, axis=0, mode=mode, _ren=_ren)
        dsi_axis_y = DSI_AXIS(self, axis=1, mode=mode, _ren=_ren)
        dsi_axis_z = DSI_AXIS(self, axis=2, mode=mode, _ren=_ren)
        self.dsi_axises = [dsi_axis_x, dsi_axis_y, dsi_axis_z]
        dsi_axis_x.sig_update.connect(self.on_update)
        dsi_axis_y.sig_update.connect(self.on_update)
        dsi_axis_z.sig_update.connect(self.on_update)

        # Picker
        self.picker = vtk.vtkVolumePicker()
        self.picker.SetTolerance(1e-6)
        self.picker.SetVolumeOpacityIsovalue(0.1)

        # TODO!!!!!!
        self.is_transform = False
        self._iren = _iren
        self._ren = _ren
        self._iren.GetInteractorStyle().AddObserver("MouseMoveEvent", self.MouseMove)
        self._iren.GetInteractorStyle().AddObserver("KeyPressEvent", self.KeyPress)
        self._iren.GetInteractorStyle().AddObserver("LeftButtonPressEvent", self.LeftButtonPress)
        self._iren.GetInteractorStyle().AddObserver("LeftButtonReleaseEvent", self.LeftButtonRelease)
        self._iren.GetInteractorStyle().AddObserver("RightButtonPressEvent", self.RightButtonPress)
        self._iren.GetInteractorStyle().AddObserver("RightButtonReleaseEvent", self.RightButtonRelease)

    def show(self):
        for a in self.get_actors():
            self._ren.AddViewProp(a)

    def get_actors(self):
        A = []
        for dsi in self.dsi_axises:
            A += dsi.get_actors()
        return A

    def interact(self, picked_actor, pos, prev_pos, cam):
        if picked_actor is self.obj_info[0]:
            displacement = np.subtract(pos, prev_pos)
            self.move_obj(displacement)

            for dsi in self.dsi_axises:
                dsi.interact(picked_actor, pos, prev_pos, cam)
        else:
            for dsi in self.dsi_axises:
                if picked_actor in dsi.get_actors():
                    self.dsi_axises[dsi.axis].interact(picked_actor, pos, prev_pos, cam)
                    break

    @QtCore.pyqtSlot(object, object)
    def on_update(self, move_info, rot_info):
        """
        move_info : displacemnet, reserved1
        rot_info : angle, rot_axis, rot_mat, cen_mat, plane_vec
        """
        for t in self.dsi_axises:
            t.update(move_info, rot_info, _sender=self.sender())

    def change_actor_status(self, picked_actor, cam):
        for i, dsi in enumerate(self.dsi_axises):

            # TODO!!!!
            _dsi_vec = dsi.line_param['vec'] / np.linalg.norm(dsi.line_param['vec'])
            _fp = cam.GetFocalPoint()
            _pos = cam.GetPosition()
            _viewvec = np.subtract(_fp, _pos)
            _viewvec = _viewvec / np.linalg.norm(_viewvec)
            inner_val = abs(np.inner(_dsi_vec, _viewvec))
            if inner_val >= 0.95:
                # dsi.actor_line.SetVisibility(False)
                dsi.actor_circle.SetVisibility(False)
                dsi.actor_c0.SetVisibility(False)
                dsi.actor_c1.SetVisibility(False)
                dsi.actor_arrow0.SetVisibility(False)
                dsi.actor_arrow1.SetVisibility(False)
            else:
                # dsi.actor_line.SetVisibility(True)
                dsi.actor_circle.SetVisibility(True)
                dsi.actor_c0.SetVisibility(True)
                dsi.actor_c1.SetVisibility(True)
                dsi.actor_arrow0.SetVisibility(True)
                dsi.actor_arrow1.SetVisibility(True)

            _plane_vec = np.cross(dsi.line_param['hor_vec'], _dsi_vec)
            inner_val = abs(np.inner(_plane_vec, _viewvec))
            if inner_val <= 0.3:
                dsi.actor_circle.SetVisibility(False)
                dsi.actor_c0.SetVisibility(False)
                dsi.actor_c1.SetVisibility(False)
            else:
                dsi.actor_circle.SetVisibility(True)
                dsi.actor_c0.SetVisibility(True)
                dsi.actor_c1.SetVisibility(True)

            if picked_actor in dsi.get_actors():
                # self.dsi_axises[i].actor_line.SetVisibility(True)
                self.dsi_axises[i].actor_line.GetProperty().SetOpacity(1)
                self.dsi_axises[i].actor_circle.GetProperty().SetOpacity(1)
            else:
                # self.dsi_axises[i].actor_line.SetVisibility(False)
                self.dsi_axises[i].actor_line.GetProperty().SetOpacity(0.15)
                self.dsi_axises[i].actor_circle.GetProperty().SetOpacity(0.1)

    def set_mode(self, mode):
        """
        :param mode:    0 : arrow is move // 1 : arrow is scale
        """
        _color = [[1, 0, 0.5], [0, 0, 1]]
        self.mode = mode
        for dsi in self.dsi_axises:
            dsi.mode = mode
            dsi.actor_arrow0.GetProperty().SetColor(_color[mode])
            dsi.actor_arrow1.GetProperty().SetColor(_color[mode])
        self._ren.GetRenderWindow().GetInteractor().Render()

    def KeyPress(self, istyle, event):
        iren = istyle.GetInteractor()
        x, y = iren.GetEventPosition()
        x, y = self.fn_convert_pos(x, y)

        if iren.GetKeySym() == 'space':
            print("Translate Mode :: ", self.is_transform)
            self.is_transform = not self.is_transform
            self.picker.Pick(x, y, 0, self._ren)
        elif iren.GetKeySym() == 'd':
            for a in self.get_test_actors():
                self._ren.RemoveActor(a)
            self.delete_test_actors()
            iren.Render()
        elif iren.GetKeySym() == 't':
            if not hasattr(self, 'ren_for_test'):
                self.test(self.ren)
            else:
                self.test(None)
                del self.ren_for_test
        elif iren.GetKeySym() == 'm':
            new_mode = 1 if self.dsi_axises[0].mode == 0 else 0
            self.set_mode(new_mode)

    def MouseMove(self, istyle, event):
        iren = istyle.GetInteractor()
        x, y = iren.GetEventPosition()
        x, y = self.fn_convert_pos(x, y)
        self._ren.SetDisplayPoint(x, y, 0)
        self._ren.DisplayToWorld()
        worldXYZ = self._ren.GetWorldPoint()

        if self.is_transform:
            prev_x, prev_y = iren.GetLastEventPosition()
            prev_x, prev_y = self.fn_convert_pos(prev_x, prev_y)
            self._ren.SetDisplayPoint(prev_x, prev_y, 0)
            self._ren.DisplayToWorld()
            prev_worldXYZ = self._ren.GetWorldPoint()
            self.interact(self.picker.GetActor(), worldXYZ, prev_worldXYZ, self._ren.GetActiveCamera())
        else:
            self.picker.Pick(x, y, 0, self._ren)
            self.change_actor_status(self.picker.GetActor(), self._ren.GetActiveCamera())
        iren.Render()

    def LeftButtonPress(self, istyle, event):
        iren = istyle.GetInteractor()
        x, y = iren.GetEventPosition()
        x, y = self.fn_convert_pos(x, y)
        __cy_state_lock = 9919
        self.picker.Pick(x, y, 0, self._ren)
        _picked_actor = self.picker.GetActor()
        if _picked_actor is self.obj_info[0]:
            self.is_transform = True
            istyle.StartState(__cy_state_lock)
            return
        for dsi in self.dsi_axises:
            if _picked_actor in dsi.get_actors():
                self.is_transform = True
                istyle.StartState(__cy_state_lock)
                return

    def LeftButtonRelease(self, istyle, event):
        iren = istyle.GetInteractor()
        x, y = iren.GetEventPosition()
        x, y = self.fn_convert_pos(x, y)
        self.is_transform = False
        istyle.StopState()

    def RightButtonPress(self, istyle, event):
        iren = istyle.GetInteractor()
        x, y = iren.GetEventPosition()
        x, y = self.fn_convert_pos(x, y)
        __cy_state_lock = 9919
        self.picker.Pick(x, y, 0, self._ren)
        _picked_actor = self.picker.GetActor()
        self.set_mode(1)
        if _picked_actor is self.obj_info[0]:
            self.is_transform = True
            istyle.StartState(__cy_state_lock)
            return
        for dsi in self.dsi_axises:
            if _picked_actor in dsi.get_actors():
                self.is_transform = True
                istyle.StartState(__cy_state_lock)
                return

    def RightButtonRelease(self, istyle, event):
        iren = istyle.GetInteractor()
        x, y = iren.GetEventPosition()
        x, y = self.fn_convert_pos(x, y)
        self.is_transform = False
        self.set_mode(0)
        istyle.StopState()

    def move_obj(self, displacement):
        tpd = self.obj_info[1]
        t = tpd.GetTransform()
        mov_mat = [[1, 0, 0, displacement[0]],
                   [0, 1, 0, displacement[1]],
                   [0, 0, 1, displacement[2]],
                   [0, 0, 0, 1]]
        _m0 = get_vtkmat_from_list(np.array(mov_mat).ravel())
        _m1 = vtk.vtkMatrix4x4()
        _m1.DeepCopy(t.GetMatrix())
        _m2 = vtk.vtkMatrix4x4()
        vtk.vtkMatrix4x4.Multiply4x4(_m0, _m1, _m2)
        t.SetMatrix(_m2)
        t.Update()
        t.Modified()
        tpd.Update()
        tpd.Modified()

    def test(self, ren):
        self.ren_for_test = ren
        for dsi in self.dsi_axises:
            dsi.test(ren)

    def get_test_actors(self):
        A = []
        for dsi in self.dsi_axises:
            A += dsi.actors_test
        return A

    def delete_test_actors(self):
        for dsi in self.dsi_axises:
            dsi.actors_test.clear()


class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        QMainWindow.__init__(self, parent)

        self.setMouseTracking(True)  # get all mouse events

        self.frame = QFrame()

        self.vl = QVBoxLayout()
        self.hl = QHBoxLayout()

        self.w2 = QWidget()
        self.vl.addWidget(self.w2)
        self.btn1 = QtWidgets.QPushButton("TEST!!!")
        self.vl.addWidget(self.btn1)
        self.w2.setLayout(self.hl)

        self.vtkWidget = QVTKRenderWindowInteractor()
        self.hl.addWidget(self.vtkWidget)

        self.ren = vtk.vtkRenderer()
        self.vtkWidget.GetRenderWindow().AddRenderer(self.ren)
        self.iren = self.vtkWidget.GetRenderWindow().GetInteractor()

        ISTYLES = cyCafe.cyVtkInteractorStyles()
        istyle1 = ISTYLES.get_vtk_interactor_style_volume_3d(0)
        self.vtkWidget._Iren.SetInteractorStyle(istyle1)

        self.init_virtual_tooth()
        self.init_dsi()

        self.ren.ResetCamera()
        self.ren.GetActiveCamera().SetParallelProjection(True)
        self.frame.setLayout(self.vl)
        self.setCentralWidget(self.frame)

        self.show()

        self.iren.Initialize()
        self.resize(600, 700)

    def init_virtual_tooth(self):
        STL_PATH = "./Wax Up Model_2.stl"
        # STL_PATH = "/Users/scott/Dicom/OD3DData/20170726/S0000000051/tmp/GroupGuide_1.stl"

        self.reader = vtk.vtkSTLReader()
        self.reader.SetFileName(STL_PATH)
        self.reader.Update()

        self.t = vtk.vtkTransform()
        self.tpd = vtk.vtkTransformPolyDataFilter()
        self.tpd.SetTransform(self.t)
        self.tpd.SetInputData(self.reader.GetOutput())
        self.tpd.Update()

        self.mapper = vtk.vtkPolyDataMapper()
        self.mapper.SetInputData(self.tpd.GetOutput())

        self.actor = vtk.vtkActor()
        self.actor.SetMapper(self.mapper)

        self.ren.AddActor(self.actor)

        color = [0.9, 0.9, 0.9]
        self.actor.GetProperty().SetColor(color)
        self.actor.GetProperty().SetAmbient(0.3)
        self.actor.GetProperty().SetAmbientColor([0.2, 0.2, 0.2])
        self.actor.GetProperty().SetDiffuse(0.5)
        self.actor.GetProperty().SetDiffuseColor(color)
        self.actor.GetProperty().SetSpecular(1)
        self.actor.GetProperty().SetSpecularPower(10)
        self.actor.GetProperty().SetSpecularColor([.95, .95, .95])

    def init_dsi(self):
        _actor = self.actor if hasattr(self, 'actor') else None
        _tpd = self.tpd if hasattr(self, 'tpd') else None

        def _convert_to_real_pos(x, y):
            """
            NOTE retina display has 2x pixel ratio
            """
            _thisScreen = Qt.QApplication.desktop().screenNumber(self.w2)
            _screen = Qt.QGuiApplication.screens()[_thisScreen]
            _ratio = _screen.devicePixelRatio()
            return x * _ratio, y * _ratio
        self.DSI = DSI_3D_OBJ([_actor, _tpd], _iren=self.iren, _ren=self.ren, _fn_convert_pos=_convert_to_real_pos)
        self.DSI.show()


if __name__ == '__main__':
    window = MainWindow()
    _qapp.exec_()
