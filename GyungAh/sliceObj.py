import vtk
from PyQt5 import QtCore, QtGui
from cyvtk import cy_vtk
import numpy as np
from PyQt5.QtGui import QIcon
from drawObj import DrawObj


class Slice(cy_vtk.Vtk_image_holder):
    class ObjectManage:

        def __init__(self):
            self.objActor = []
            self.obj = []

    class objectType:
        Ruler = 0
        TapeLine = 1
        Angle = 2
        Profile = 3
        Area = 4
        Roi = 5
        Arrow = 6
        Note = 7
        Delete = 8

    class SliceOrientation:
        axial = 0
        coronal = 1
        sagittal = 2

    def __init__(self , slice_orientation, dicom):
        super().__init__()
        self.pickerX = 0.0
        self.pickerY = 0.0
        self.preX = 0.0
        self.preY = 0.0
        self.preZ = 0.0
        self.curX = 0.0
        self.curY = 0.0
        self.curZ = 0.0
        self.color = [255, 0, 0]
        self.root_manage_obj = self.ObjectManage()
        self.curObj = ''
        self.preObjActor = ''
        self.press = False
        self.picker = vtk.vtkVolumePicker()
        self.downCount = 0

        self.rulerToggle = False
        self.tapeLineToggle = False
        self.angleToggle = False
        self.profileToggle = False
        self.areaToggle = False
        self.roiToggle = False
        self.arrowToggle = False
        self.noteToggle = False
        self.deleteToggle = False

        self.im = vtk.vtkImageResliceMapper()
        self.ip = vtk.vtkImageProperty()
        self.ia = vtk.vtkImageSlice()
        self.plane = vtk.vtkPlane()
        self.slice_orientation = slice_orientation
        self.dicom = dicom

        # initial interval
        self.thickness = dicom.img_spacing[2]
        extent = self.dicom.extent
        dim = self.dicom.dim
        self.__camera_positions_dict = {
            0: ((self.dicom.center[0], self.dicom.center[1], self.dicom.center[2]), (0, 0, (extent[0] + dim[2] - 200)), (0, -1, 0)),  # Axial
            1: ((self.dicom.center[0], self.dicom.center[1], self.dicom.center[2]), (0, (extent[2] + dim[1] - 200), 0), (0, 0, 1)),   # Coronal
            2: ((self.dicom.center[0], self.dicom.center[1], self.dicom.center[2]), ((extent[4] + dim[0] + 200), 0, 0), (0, 0, 1)),   # Sagittal
        }

        if self.slice_orientation == self.SliceOrientation.axial:
            self.vec_plane = [0, 0, 1]
        elif self.slice_orientation == self.SliceOrientation.coronal:
            self.vec_plane = [0, 1, 0]
        elif self.slice_orientation == self.SliceOrientation.sagittal:
            self.vec_plane = [1, 0, 0]

        self.im.SetInputConnection(self.dicom.image.GetOutputPort())
        self.plane.SetNormal(self.vec_plane)

        self.plane.SetOrigin(self.dicom.center)
        self.im.SetSlicePlane(self.plane)

        self.ip.SetColorWindow(2000)
        self.ip.SetColorLevel(1000)
        self.ip.SetAmbient(0.0)
        self.ip.SetDiffuse(1.0)
        self.ip.SetOpacity(1.0)
        self.ip.SetInterpolationTypeToLinear()

        self.ia.SetMapper(self.im)
        self.ia.SetProperty(self.ip)

        self.reset_camera(slice_orientation, self.ren)
        self.ren.AddViewProp(self.ia)

    def reset_camera(self, position, ren):  # 0: initial 3d view, 1: left, 2: right, 3: front, 4: back, 5: top, 6: bottom
        focal, position, viewup = self.__camera_positions_dict[position]
        cam1 = ren.GetActiveCamera()
        cam1.SetFocalPoint(focal)
        cam1.SetPosition(position)
        cam1.SetViewUp(viewup)
        ren.ResetCameraClippingRange()

    def wheelEvent(self, e):
        d = e.angleDelta().y()
        self.istyle.SetMouseWheelMotionFactor(abs(d) / 120)
        self.move_plane(e)

    def move_plane(self, event):
        # calculate Hor/Ver Vector
        normalVector = self.vec_plane

        if self.slice_orientation == self.SliceOrientation.axial:
            upVector = [0, -1, 0]
        elif self.slice_orientation == self.SliceOrientation.coronal or self.slice_orientation == self.SliceOrientation.sagittal:
            upVector = [0, 0, 1]

        newVec1 = [0] * 3
        vtk.vtkMath.Cross(normalVector, upVector, newVec1)
        newVec2 = [0] * 3
        vtk.vtkMath.Cross(normalVector, newVec1, newVec2)

        d = event.angleDelta().y()
        self.istyle.SetMouseWheelMotionFactor(abs(d) / 120)

        if d >= 0:
            direction = 1
        else:
            direction = -1
        interval = self.thickness * direction

        # generate interval(thick) vector to axis
        interval_mat = [0] * 3
        interval_mat[self.slice_orientation] = 1 * interval

        if self.slice_orientation == self.SliceOrientation.axial:
            V = np.array([normalVector, newVec1, newVec2]).transpose()
        elif self.slice_orientation == self.SliceOrientation.coronal:
            V = np.array([newVec1, normalVector, newVec2]).transpose()
        elif self.slice_orientation == self.SliceOrientation.sagittal:
            V = np.array([newVec1, newVec2, normalVector]).transpose()

        newXYZ = np.dot(V, interval_mat)
        self.plane.SetOrigin(self.plane.GetOrigin() + newXYZ)
        self.im.SetSlicePlane(self.plane)
        self.view._RenderWindow.Render()

    def mousePressEvent(self, e):

        if e.buttons() == QtCore.Qt.LeftButton:  # left click grows radius
            pass
        else:  # right click shrinks radius
            pass
        self.downCount += 1
        if self.curObj:
            self.curObj.mousePress(self, e)

    def mouseMoveEvent(self, e):
        if self.rulerToggle == False and self.tapeLineToggle == False and self.angleToggle == False and self.profileToggle == False and self.areaToggle == False and self.roiToggle == False and self.arrowToggle == False and self.noteToggle == False and self.deleteToggle == False:
            return

        self.pickerX = e.pos().x()
        self.pickerY = self.height() - e.pos().y()

        self.picker.Pick(self.pickerX, self.pickerY, 0, self.ren)
        p = self.picker.GetPickPosition()

        self.curX = self.pickerX / self.width()
        self.curY = self.pickerY / self.height()
        self.curZ = p[2]

        if self.curObj:
            self.curObj.mouse_move(self, e)

    def mouseReleaseEvent(self, e):

        if self.rulerToggle == False and self.tapeLineToggle == False and self.angleToggle == False and self.profileToggle == False and self.areaToggle == False and self.roiToggle == False and self.arrowToggle == False and self.noteToggle == False and self.deleteToggle == False:
            return

        if self.downCount == 1:
            self.pickerX = e.pos().x()
            self.pickerY = self.height() - e.pos().y()

            self.picker.Pick(self.pickerX, self.pickerY, 0, self.ren)
            p = self.picker.GetPickPosition()

            self.preX = self.pickerX / self.width()
            self.preY = self.pickerY / self.height()
            self.preZ = p[2]

            units = [0] * 3
            units[0] = self.width() * self.dicom.img_spacing[0]
            units[1] = self.height() * self.dicom.img_spacing[1]
            units[2] = 1 * self.dicom.img_spacing[2]

            if self.rulerToggle == True:
                self.curObj = DrawObj(self.preX, self.preY, self.preZ, self.curX, self.curY, self.curZ, self.color, self.objectType.Ruler, units)
            elif self.tapeLineToggle == True:
                self.curObj = DrawObj(self.preX, self.preY, self.preZ, self.curX, self.curY, self.curZ, self.color, self.objectType.TapeLine, units)
            elif self.angleToggle == True:
                self.curObj = DrawObj(self.preX, self.preY, self.preZ, self.curX, self.curY, self.curZ, self.color, self.objectType.Angle, units)
            elif self.profileToggle == True:
                self.curObj = DrawObj(self.preX, self.preY, self.preZ, self.curX, self.curY, self.curZ, self.color, self.objectType.Profile, units)
            elif self.areaToggle == True:
                self.curObj = DrawObj(self.preX, self.preY, self.preZ, self.curX, self.curY, self.curZ, self.color, self.objectType.Area, units)
            else:
                pass
        if self.curObj:
            self.curObj.mouseRealse(self, e)

    def make_profile(self):
        self.im