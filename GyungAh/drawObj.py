
from vtk import *
import math
import numpy as np
from math import sqrt
from PyQt5 import QtCore


class DrawObj():

    class ObjectType:
        Ruler = 0
        TapeLine = 1
        Angle = 2
        Profile = 3
        Area = 4
        Roi = 5
        Arrow = 6
        Note = 7
        Delete = 8

    def __init__(self, preX, preY, preZ, curX, curY, curZ, color, type, units):

        self.preX = preX
        self.preY = preY
        self.preZ = preZ

        self.curX = curX
        self.curY = curY
        self.curZ = curZ

        self.color = color
        self.type = type

        self.lineDistanse = 0.0
        self.angle = 0.0
        self.units = units

        self.posX_list = []
        self.posY_list = []
        self.posZ_list = []

    def mousePress(self, _screen, e):
        if e.buttons() == QtCore.Qt.LeftButton:  # left click grows radius
            pass
        elif e.buttons() == QtCore.Qt.RightButton:
            if self.type == self.ObjectType.Ruler:
                if _screen.preObjActor:
                    _screen.ren.RemoveViewProp(_screen.preObjActor)
                    _screen.preObjActor = ''
                _screen.downCount = 0
                _screen.view._RenderWindow.Render()
            elif self.type == self.ObjectType.TapeLine:
                if _screen.preObjActor:
                    _screen.ren.RemoveViewProp(_screen.preObjActor)
                    _screen.preObjActor = ''
                _screen.downCount = 0
                _screen.view._RenderWindow.Render()
            elif self.type == self.ObjectType.Angle:
                if _screen.preObjActor:
                    _screen.ren.RemoveViewProp(_screen.preObjActor)
                    _screen.preObjActor = ''
                _screen.downCount = 0
                _screen.view._RenderWindow.Render()

            elif self.type == self.ObjectType.Angle:
                if _screen.preObjActor:
                    _screen.ren.RemoveViewProp(_screen.preObjActor)
                    _screen.preObjActor = ''
                _screen.downCount = 0
                _screen.view._RenderWindow.Render()

            elif self.type == self.ObjectType.Profile:
                if _screen.preObjActor:
                    _screen.ren.RemoveViewProp(_screen.preObjActor)
                    _screen.preObjActor = ''
                _screen.downCount = 0
                _screen.view._RenderWindow.Render()

            elif self.type == self.ObjectType.Area:
                if _screen.downCount > 2:

                    text_actor = vtk.vtkTextActor()
                    area_value = self.poly_area(self.posX_list, self.posY_list, self.posZ_list)
                    string = str(area_value)
                    text_actor.SetInput('[Area] '+string + ' mm2')
                    text_actor.GetTextProperty().SetColor((0, 1, 1))
                    text_actor.GetPositionCoordinate().SetCoordinateSystemToNormalizedViewport()

                    pos_x = (self.preX + self.curX) / 2
                    pos_y = (self.preY + self.curY) / 2
                    text_actor.SetPosition(pos_x, pos_y)

                    _screen.ren.AddViewProp(text_actor)

                    self.setCur(self.posX_list[0], self.posY_list[0], self.posZ_list[0])
                    line = self.draw_line()

                    if not _screen.preObjActor == '':
                        _screen.ren.RemoveViewProp(_screen.preObjActor)

                    _screen.ren.AddViewProp(line)
                    _screen.view._RenderWindow.Render()

                    _screen.root_manage_obj.objActor.append(_screen.preObjActor)
                    _screen.root_manage_obj.obj.append(self)

                    if _screen.preObjActor:
                        _screen.ren.RemoveViewProp(_screen.preObjActor)
                        _screen.preObjActor = ''
                    _screen.downCount = 0
                    _screen.view._RenderWindow.Render()


    def mouseRealse(self, _screen, e):

        if self.type == self.ObjectType.Ruler: #self.rulerToggle == True:
            if _screen.downCount == 2:
                text_actor = vtk.vtkTextActor()
                dis = self.GetDistance()
                string = str(dis)
                text_actor.SetInput(string + 'mm')
                text_actor.GetTextProperty().SetColor((0, 1, 1))
                text_actor.GetPositionCoordinate().SetCoordinateSystemToNormalizedViewport()

                pos_x = (self.preX + self.curX) / 2
                pos_y = (self.preY + self.curY) / 2
                text_actor.SetPosition(pos_x, pos_y)

                _screen.ren.AddViewProp(text_actor)
                _screen.view._RenderWindow.Render()
                _screen.downCount = 0

                self.posX_list.append(self.preX)
                self.posX_list.append(self.curX)
                self.posY_list.append(self.preY)
                self.posY_list.append(self.curY)
                self.posZ_list.append(self.preZ)
                self.posZ_list.append(self.curZ)
                _screen.root_manage_obj.objActor.append(_screen.preObjActor)
                _screen.root_manage_obj.obj.append(self)

                # don't remove current obj
                _screen.preObjActor = ''

        elif self.type == self.ObjectType.TapeLine:
            if _screen.downCount == 1:
                self.posX_list.append(self.preX)
                self.posY_list.append(self.preY)
                self.posZ_list.append(self.preZ)
            if _screen.downCount > 1:
                self.posX_list.append(self.curX)
                self.posY_list.append(self.curY)
                self.posZ_list.append(self.curZ)

                text_actor = vtk.vtkTextActor()
                dis = self.GetDistance()
                string = str(dis)
                text_actor.SetInput(string + 'mm')
                text_actor.GetTextProperty().SetColor((0, 1, 1))
                text_actor.GetPositionCoordinate().SetCoordinateSystemToNormalizedViewport()

                pos_x = (self.preX + self.curX) / 2
                pos_y = (self.preY + self.curY) / 2
                text_actor.SetPosition(pos_x, pos_y)

                _screen.ren.AddViewProp(text_actor)
                _screen.view._RenderWindow.Render()


                _screen.root_manage_obj.objActor.append(_screen.preObjActor)
                _screen.root_manage_obj.obj.append(self)

                self.setPre(self.curX, self.curY, self.curZ)
                # don't remove current obj
                _screen.preObjActor = ''

        elif self.type == self.ObjectType.Angle:
            if _screen.downCount == 2:
                self.posX_list.append(self.preX)
                self.posX_list.append(self.curX)
                self.posY_list.append(self.preY)
                self.posY_list.append(self.curY)
                self.posZ_list.append(self.preZ)
                self.posZ_list.append(self.curZ)
                _screen.preObjActor = ''
                self.setCur(self.preX, self.preY, self.preZ)

            elif _screen.downCount == 3:
                self.posX_list.append(self.curX)
                self.posY_list.append(self.curY)
                self.posZ_list.append(self.curZ)
                text_actor = vtk.vtkTextActor()

                A = np.array([self.posX_list[1], self.posY_list[1], self.posZ_list[1]])
                B = np.array([self.posX_list[0], self.posY_list[0], self.posZ_list[0]])
                C = np.array([self.posX_list[2], self.posY_list[2], self.posZ_list[2]])
                self.angle = round(self.find_angle(A, B, C), 1)
                string = str(round(self.angle, 1))
                text_actor.SetInput(string + '°')
                text_actor.GetTextProperty().SetColor((0, 1, 1))
                text_actor.GetPositionCoordinate().SetCoordinateSystemToNormalizedViewport()

                pos_x = (A[0] + C[0]) / 2
                pos_y = (A[1] + C[1]) / 2
                text_actor.SetPosition(pos_x, pos_y)

                _screen.ren.AddViewProp(text_actor)
                _screen.view._RenderWindow.Render()

                _screen.downCount = 0
                _screen.preObjActor = ''

                _screen.root_manage_obj.objActor.append(_screen.preObjActor)
                _screen.root_manage_obj.obj.append(self)

        elif self.type == self.ObjectType.Profile:
            if _screen.downCount == 2:
                text_actor = vtk.vtkTextActor()
                dis = self.GetDistance()
                string = str(dis)
                text_actor.SetInput('[Profile] '+string+ ' mm')
                text_actor.GetTextProperty().SetColor((0, 1, 1))
                text_actor.GetPositionCoordinate().SetCoordinateSystemToNormalizedViewport()

                pos_x = (self.preX + self.curX) / 2
                pos_y = (self.preY + self.curY) / 2
                text_actor.SetPosition(pos_x, pos_y)

                _screen.ren.AddViewProp(text_actor)
                _screen.view._RenderWindow.Render()
                _screen.downCount = 0

                self.posX_list.append(self.preX)
                self.posX_list.append(self.curX)
                self.posY_list.append(self.preY)
                self.posY_list.append(self.curY)
                self.posZ_list.append(self.preZ)
                self.posZ_list.append(self.curZ)
                _screen.root_manage_obj.objActor.append(_screen.preObjActor)
                _screen.root_manage_obj.obj.append(self)

                # don't remove current obj
                _screen.preObjActor = ''

        elif self.type == self.ObjectType.Area:
            if _screen.downCount == 1:
                self.posX_list.append(self.preX)
                self.posY_list.append(self.preY)
                self.posZ_list.append(self.preZ)
            if _screen.downCount > 1:
                self.posX_list.append(self.curX)
                self.posY_list.append(self.curY)
                self.posZ_list.append(self.curZ)
                _screen.root_manage_obj.objActor.append(_screen.preObjActor)
                _screen.root_manage_obj.obj.append(self)

                self.setPre(self.curX, self.curY, self.curZ)
                # don't remove current obj
                _screen.preObjActor = ''
        else :
             pass

    def mouse_move(self, _screen, e):

        if self.type == self.ObjectType.Ruler: #self.rulerToggle == True:

            if _screen.downCount == 1:
                self.setCur(_screen.curX, _screen.curY, _screen.curZ)
                ruler = self.draw_line()

                if not _screen.preObjActor == '':
                    _screen.ren.RemoveViewProp(_screen.preObjActor)

                _screen.ren.AddViewProp(ruler)
                _screen.preObjActor = ruler

        elif self.type == self.ObjectType.TapeLine:
            if _screen.downCount > 0:
                self.setCur(_screen.curX, _screen.curY, _screen.curZ)
                line = self.draw_line()

                if not _screen.preObjActor == '':
                    _screen.ren.RemoveViewProp(_screen.preObjActor)

                _screen.ren.AddViewProp(line)
                _screen.preObjActor = line

        elif self.type == self.ObjectType.Angle:
            if _screen.downCount > 0:
                self.setCur(_screen.curX, _screen.curY, _screen.curZ)
                line = self.draw_line()

                if not _screen.preObjActor == '':
                    _screen.ren.RemoveViewProp(_screen.preObjActor)

                _screen.ren.AddViewProp(line)
                _screen.preObjActor = line

        elif self.type == self.ObjectType.Profile:
            if _screen.downCount == 1:
                self.setCur(_screen.curX, _screen.curY, _screen.curZ)
                profile_line = self.draw_line()

                if not _screen.preObjActor == '':
                    _screen.ren.RemoveViewProp(_screen.preObjActor)

                _screen.ren.AddViewProp(profile_line)
                _screen.preObjActor = profile_line
        elif self.type == self.ObjectType.Area:
            if _screen.downCount > 0:
                self.setCur(_screen.curX, _screen.curY, _screen.curZ)
                line = self.draw_line()

                if not _screen.preObjActor == '':
                    _screen.ren.RemoveViewProp(_screen.preObjActor)

                _screen.ren.AddViewProp(line)
                _screen.preObjActor = line
        _screen.view._RenderWindow.Render()

    def SetUnit(self, units):
        self.units = units

    def GetUnit(self):
        return self.units

    def GetDistance(self):
        return self.lineDistanse

    def setCur(self, cur_x, cur_y, cur_z):
        self.curX = cur_x
        self.curY = cur_y
        self.curZ = cur_z

    def setPre(self, pre_x, pre_y, pre_z):
        self.preX = pre_x
        self.preY = pre_y
        self.preZ = pre_z

    def math_calc_dist(self, p1, p2):
        return math.sqrt(math.pow((p2[0] - p1[0]) * self.units[0], 2) +
                          math.pow((p2[1] - p1[1]) * self.units[1], 2) +
                          math.pow((p2[2] - p1[2]) * self.units[2], 2))

    # area of polygon poly
    def poly_area(self, polyX, polyY, polyZ):
        if len(polyX) < 3:  # not a plane - no area
            return 0
        total = [0, 0, 0]
        N = len(polyX)
        poly = []
        for i in range(N):
            poly.append([polyX[i] * self.units[0], polyY[i] * self.units[1], polyZ[i] * self.units[2]])

        for i in range(N):
            vi1 = poly[i]
            vi2 = poly[(i + 1) % N]
            prod = np.cross(vi1, vi2)
            total[0] += prod[0]
            total[1] += prod[1]
            total[2] += prod[2]
        result = np.dot(total, self.unit_normal(poly[0], poly[1], poly[2]))
        return round(abs(result / 2), 2)

    # unit normal vector of plane defined by points a, b, and c
    def unit_normal(self, a, b, c):
        x = np.linalg.det([[1, a[1], a[2]],
                           [1, b[1], b[2]],
                           [1, c[1], c[2]]])
        y = np.linalg.det([[a[0], 1, a[2]],
                           [b[0], 1, b[2]],
                           [c[0], 1, c[2]]])
        z = np.linalg.det([[a[0], a[1], 1],
                           [b[0], b[1], 1],
                           [c[0], c[1], 1]])
        magnitude = (x ** 2 + y ** 2 + z ** 2) ** .5
        return (x / magnitude, y / magnitude, z / magnitude)

    # A first point, C second point, B center point
    def find_angle(self, A, B, C):
        AB = math.sqrt(math.pow((B[0] - A[0]) * self.units[0], 2) + math.pow((B[1] - A[1]) * self.units[1], 2) + math.pow((B[2] - A[2]) * self.units[2], 2))
        BC = math.sqrt(math.pow((B[0] - C[0]) * self.units[0] , 2) + math.pow((B[1] - C[1]) * self.units[1], 2) + math.pow((B[2] - C[2]) * self.units[2], 2))
        AC = math.sqrt(math.pow((C[0] - A[0]) * self.units[0], 2) + math.pow((C[1] - A[1]) * self.units[1], 2) + math.pow((C[2] - A[2]) * self.units[2], 2))
        angle = math.acos((BC * BC + AB * AB - AC * AC) / (2 * BC * AB))
        return angle * (180 / np.pi)

    def draw_line(self):

        line = vtkLineSource()
        line.SetPoint1(self.preX, self.preY, 0)
        line.SetPoint2(self.curX, self.curY, 0)

        coord = vtkCoordinate()
        coord.SetCoordinateSystemToNormalizedDisplay()

        # Create mappers
        mapper = vtkPolyDataMapper2D()
        mapper.SetInputConnection(line.GetOutputPort())
        mapper.SetTransformCoordinate(coord)

        # Create actors
        actor = vtkActor2D() #vtkActor()
        actor.SetMapper(mapper)
        actor.GetProperty().SetColor(self.color[0], self.color[1], self.color[2])

        a = np.array([self.preX, self.preY, self.preZ])
        b = np.array([self.curX, self.curY, self.curZ])
        self.lineDistanse = round(self.math_calc_dist(a, b), 2)

        return actor



