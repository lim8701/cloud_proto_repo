
from vtk import *
import math
import numpy as np
from math import sqrt


class drawObj():

    def __init__(self, preX, preY, preZ, curX, curY, curZ, color,type, nextNode):


        self.preX = preX
        self.preY = preY
        self.preZ = preZ

        self.curX = curX
        self.curY = curY
        self.curZ = curZ
        self.color = color

        self.type = type
        self.nextNode = nextNode
        self.lineDistanse =0

    def setDistance(self):
        return self.lineDistanse



    def setCur(self,curX, curY, curZ):
        self.curX = curX
        self.curY = curY
        self.curZ = curZ

    def math_calc_dist(self, p1, p2):
        return math.sqrt(math.pow((p2[0] - p1[0]), 2) +
                         math.pow((p2[1] - p1[1]), 2) +
                         math.pow((p2[2] - p1[2]), 2))

    def drawLine(self):

        line = vtkLineSource()
        line.SetPoint1(self.preX, self.preY, 0)
        line.SetPoint2(self.curX, self.curY, 0)

        # Create mappers
        mapper = vtkPolyDataMapper()
        mapper.SetInputConnection(line.GetOutputPort())
        mapper.Update()

        # Create actors
        actor = vtkActor()
        actor.SetMapper(mapper)
        actor.GetProperty().SetColor(self.color[0], self.color[1], self.color[2])

        a = np.array([self.preX, self.preY, self.preZ])
        b = np.array([self.curX, self.curY, self.curZ])
        self.lineDistanse = self.math_calc_dist(a,b)

        return actor



