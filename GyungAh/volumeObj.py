import vtk
import SimpleITK as sitk
from cyvtk import cy_vtk


class ReadDIcom():
    def __init__(self, dicom_path):
        self.reader = ''
        self.img_spacing = [0]*3
        self.dim = [0]*3
        self.center = [0]*3
        self.image = ''
        self.read(dicom_path)

    def read(self, path):
        # GDCM Reader
        self.reader = sitk.ImageSeriesReader()
        paths = self.reader.GetGDCMSeriesFileNames(path)
        self.reader.SetFileNames(paths)
        img = sitk.ReadImage(paths)

        # Convert to vtkImage
        data = sitk.GetArrayFromImage(img)

        self.img_spacing = img.GetSpacing()
        img_cen = img.GetOrigin()

        img_data = data.astype('int16')
        img_string = img_data.tostring()
        self.dim = data.shape

        importer = vtk.vtkImageImport()
        importer.CopyImportVoidPointer(img_string, len(img_string))
        importer.SetNumberOfScalarComponents(1)

        self.extent = importer.GetDataExtent()

        importer.SetDataExtent(self.extent[0], self.extent[0] + self.dim[2] - 1,
                               self.extent[2], self.extent[2] + self.dim[1] - 1,
                               self.extent[4], self.extent[4] + self.dim[0] - 1)
        importer.SetWholeExtent(self.extent[0], self.extent[0] + self.dim[2] - 1,
                                self.extent[2], self.extent[2] + self.dim[1] - 1,
                                self.extent[4], self.extent[4] + self.dim[0] - 1)

        importer.SetDataSpacing(self.img_spacing[0], self.img_spacing[1], self.img_spacing[2])
        importer.SetDataOrigin(img_cen)

        self.center = [img_cen[0] + self.img_spacing[0] * 0.5 * (self.extent[0] + self.extent[0] + self.dim[2] - 1),
                       img_cen[1] + self.img_spacing[1] * 0.5 * (self.extent[2] + self.extent[2] + self.dim[1] - 1),
                       img_cen[2] + self.img_spacing[2] * 0.5 * (self.extent[4] + self.extent[4] + self.dim[0] - 1)]

        self.image = importer


class VolumeScreen(cy_vtk.Vtk_image_holder):

    def __init__(self, dicom):
        super().__init__()
        self.dicom = dicom
        #self.image = dicom.image
        self.opacityWindow = 4096 #opacityWindow #4096
        self.opacityLevel = 2048 #opacityLevel #2048
        self.volume =''

        self.SetVolumeAttribute()

    #def SetDicom(self, img):
    #    self.image = img

    def GetDimInfo(self):
        return self.dim

    def GetImage(self):
        return self.image

    def SetWindow(self, window):
        self.opacityWindow = window

    def SetLevel(self, level):
        self.opacityLevel = level

    def SetVolumeAttribute(self):
        # Volume
        colorFun = vtk.vtkColorTransferFunction()
        opacityFun = vtk.vtkPiecewiseFunction()
        property = vtk.vtkVolumeProperty()
        mapper = vtk.vtkSmartVolumeMapper()
        self.volume = vtk.vtkVolume()

        # Mapper
        mapper.SetInputConnection( self.dicom.image.GetOutputPort())
        mapper.SetBlendModeToMaximumIntensity()

        # property
        property.SetIndependentComponents(True)
        colorFun.AddRGBSegment(0.0, 1.0, 1.0, 1.0, 255.0, 1.0, 1.0, 1.0)

        opacityFun.AddSegment(self.opacityLevel - 0.5 * self.opacityWindow, 0.0,
                              self.opacityLevel + 0.5 * self.opacityWindow, 1.0)
        property.SetColor(colorFun)
        property.SetScalarOpacity(opacityFun)
        property.SetInterpolationTypeToLinear()

        # volume
        self.volume.SetMapper(mapper)
        self.volume.SetProperty(property)

        self.ren.AddViewProp(self.volume)

        # Volume Camera
        camera_volume = self.ren.GetActiveCamera()
        center_pos = self.volume.GetCenter()
        camera_volume.SetFocalPoint(center_pos[0], center_pos[1], center_pos[2])
        camera_volume.SetPosition(center_pos[0], center_pos[1] - 400, center_pos[2])
        camera_volume.SetViewUp(0, 0, 1)


