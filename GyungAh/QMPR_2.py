import sys
import vtk
from PyQt5 import QtCore, QtGui
from PyQt5.QtWidgets import *
from PyQt5.QtGui import QIcon
from sliceObj import Slice
from volumeObj import VolumeScreen, ReadDIcom
from qapp import _qapp



class MainWindow(QMainWindow):

    class SliceOrientation:
        axial = 0
        coronal = 1
        sagittal = 2

    def initMenu (self):

        self.__menu_dict = {
            0: ('Ruler.png', '&Ruler', 'Ctrl+R', 'Measure: Ruler', self.menuRulerCheck),
            1: ('Tapeline.png', '&Tapeline', 'Ctrl+T', 'Measure: Tapeline', self.menuTapeLineCheck),
            2: ('Angle.png', '&Angle', 'Ctrl+A', 'Measure: Angle', self.menuAngleCheck),
            3: ('Profile.png', '&Profile', 'Ctrl+P', 'Measure: Profile', self.menuProfileCheck),
            4: ('Area.png', '&Area', 'Ctrl+M', 'Measure: Area', self.menuAreaCheck),
            5: ('Roi.png', '&Roi', 'Ctrl+O', 'Measure: Roi', self.menuRoiCheck),
            6: ('Arrow.png', '&Arrow', 'Ctrl+Q', 'Measure: Arrow', self.menuArrowCheck),
            7: ('Note.png', '&Note', 'Ctrl+N', 'Measure: Note', self.menuNoteCheck),
            8: ('Delete.png', '&Delete', 'Ctrl+D', 'Measure: Delete', self.menuDeleteCheck)
        }
        menubar = self.menuBar()
        MeasureMenu = menubar.addMenu('&Measure')

        for i in range(9):
            Icon, menuName, key, tip, fun = self.__menu_dict[i]
            Action = QAction(QIcon(Icon), menuName, self)
            Action.setShortcut(key)
            Action.setStatusTip(tip)
            Action.triggered.connect(fun)
            MeasureMenu.addAction(Action)

    def menuRulerCheck(self):
        if self.slice_axial.rulerToggle == False:
            self.slice_axial.rulerToggle = True
            self.slice_axial.tapeLineToggle = False
            self.slice_axial.angleToggle = False
            self.slice_axial.profileToggle = False
            self.slice_axial.areaToggle = False
            self.slice_axial.roiToggle = False
            self.slice_axial.arrowToggle = False
            self.slice_axial.noteToggle = False
            self.slice_axial.deleteToggle = False
        else:
            self.slice_axial.rulerToggle = False

        if self.slice_coronal.rulerToggle == False:
            self.slice_coronal.rulerToggle = True
            self.slice_coronal.tapeLineToggle = False
            self.slice_coronal.angleToggle = False
            self.slice_coronal.profileToggle = False
            self.slice_coronal.areaToggle = False
            self.slice_coronal.roiToggle = False
            self.slice_coronal.arrowToggle = False
            self.slice_coronal.noteToggle = False
            self.slice_coronal.deleteToggle = False
        else:
            self.slice_coronal.rulerToggle = False

        if self.slice_sagittal.rulerToggle == False:
            self.slice_sagittal.rulerToggle = True
            self.slice_sagittal.tapeLineToggle = False
            self.slice_sagittal.angleToggle = False
            self.slice_sagittal.profileToggle = False
            self.slice_sagittal.areaToggle = False
            self.slice_sagittal.roiToggle = False
            self.slice_sagittal.arrowToggle = False
            self.slice_sagittal.noteToggle = False
            self.slice_sagittal.deleteToggle = False
        else:
            self.slice_sagittal.rulerToggle = False
        '''
        if self.volume.rulerToggle == False:
            self.volume.rulerToggle = True
        else:
            self.volume.rulerToggle = False
        '''
    def menuTapeLineCheck(self):
        if self.slice_axial.tapeLineToggle == False:
            self.slice_axial.rulerToggle = False
            self.slice_axial.tapeLineToggle = True
            self.slice_axial.angleToggle = False
            self.slice_axial.profileToggle = False
            self.slice_axial.areaToggle = False
            self.slice_axial.roiToggle = False
            self.slice_axial.arrowToggle = False
            self.slice_axial.noteToggle = False
            self.slice_axial.deleteToggle = False
        else:
            self.slice_axial.tapeLineToggle = False

        if self.slice_coronal.tapeLineToggle == False:
            self.slice_coronal.rulerToggle = False
            self.slice_coronal.tapeLineToggle = True
            self.slice_coronal.angleToggle = False
            self.slice_coronal.profileToggle = False
            self.slice_coronal.areaToggle = False
            self.slice_coronal.roiToggle = False
            self.slice_coronal.arrowToggle = False
            self.slice_coronal.noteToggle = False
            self.slice_coronal.deleteToggle = False
        else:
            self.slice_coronal.tapeLineToggle = False

        if self.slice_sagittal.tapeLineToggle == False:
            self.slice_sagittal.rulerToggle = False
            self.slice_sagittal.tapeLineToggle = True
            self.slice_sagittal.angleToggle = False
            self.slice_sagittal.profileToggle = False
            self.slice_sagittal.areaToggle = False
            self.slice_sagittal.roiToggle = False
            self.slice_sagittal.arrowToggle = False
            self.slice_sagittal.noteToggle = False
            self.slice_sagittal.deleteToggle = False
        else:
            self.slice_sagittal.tapeLineToggle = False
        '''
        if self.volume.tapeLineToggle == False:
            self.volume.tapeLineToggle = True
        else:
            self.volume.tapeLineToggle = False
        '''
    def menuAngleCheck(self):
        if self.slice_axial.angleToggle == False:
            self.slice_axial.rulerToggle = False
            self.slice_axial.tapeLineToggle = False
            self.slice_axial.angleToggle = True
            self.slice_axial.profileToggle = False
            self.slice_axial.areaToggle = False
            self.slice_axial.roiToggle = False
            self.slice_axial.arrowToggle = False
            self.slice_axial.noteToggle = False
            self.slice_axial.deleteToggle = False
        else:
            self.slice_axial.angleToggle = False

        if self.slice_coronal.angleToggle == False:
            self.slice_coronal.rulerToggle = False
            self.slice_coronal.tapeLineToggle = False
            self.slice_coronal.angleToggle = True
            self.slice_coronal.profileToggle = False
            self.slice_coronal.areaToggle = False
            self.slice_coronal.roiToggle = False
            self.slice_coronal.arrowToggle = False
            self.slice_coronal.noteToggle = False
            self.slice_coronal.deleteToggle = False
        else:
            self.slice_coronal.angleToggle = False

        if self.slice_sagittal.angleToggle == False:
            self.slice_sagittal.rulerToggle = False
            self.slice_sagittal.tapeLineToggle = False
            self.slice_sagittal.angleToggle = True
            self.slice_sagittal.profileToggle = False
            self.slice_sagittal.areaToggle = False
            self.slice_sagittal.roiToggle = False
            self.slice_sagittal.arrowToggle = False
            self.slice_sagittal.noteToggle = False
            self.slice_sagittal.deleteToggle = False
        else:
            self.slice_sagittal.angleToggle = False
        '''
        if self.volume.angleToggle == False:
            self.volume.angleToggle = True
        else:
            self.volume.angleToggle = False
        '''
    def menuProfileCheck(self):
        if self.slice_axial.profileToggle == False:
            self.slice_axial.rulerToggle = False
            self.slice_axial.tapeLineToggle = False
            self.slice_axial.angleToggle = False
            self.slice_axial.profileToggle = True
            self.slice_axial.areaToggle = False
            self.slice_axial.roiToggle = False
            self.slice_axial.arrowToggle = False
            self.slice_axial.noteToggle = False
            self.slice_axial.deleteToggle = False
        else:
            self.slice_axial.profileToggle = False

        if self.slice_coronal.profileToggle == False:
            self.slice_coronal.rulerToggle = False
            self.slice_coronal.tapeLineToggle = False
            self.slice_coronal.angleToggle = False
            self.slice_coronal.profileToggle = True
            self.slice_coronal.areaToggle = False
            self.slice_coronal.roiToggle = False
            self.slice_coronal.arrowToggle = False
            self.slice_coronal.noteToggle = False
            self.slice_coronal.deleteToggle = False
        else:
            self.slice_coronal.profileToggle = False

        if self.slice_sagittal.profileToggle == False:
            self.slice_sagittal.rulerToggle = False
            self.slice_sagittal.tapeLineToggle = False
            self.slice_sagittal.angleToggle = False
            self.slice_sagittal.profileToggle = True
            self.slice_sagittal.areaToggle = False
            self.slice_sagittal.roiToggle = False
            self.slice_sagittal.arrowToggle = False
            self.slice_sagittal.noteToggle = False
            self.slice_sagittal.deleteToggle = False
        else:
            self.slice_sagittal.profileToggle = False
        '''
        if self.volume.profileToggle == False:
            self.volume.profileToggle = True
        else:
            self.volume.profileToggle = False
        '''
    def menuAreaCheck(self):
        if self.slice_axial.areaToggle == False:
            self.slice_axial.rulerToggle = False
            self.slice_axial.tapeLineToggle = False
            self.slice_axial.angleToggle = False
            self.slice_axial.profileToggle = False
            self.slice_axial.areaToggle = True
            self.slice_axial.roiToggle = False
            self.slice_axial.arrowToggle = False
            self.slice_axial.noteToggle = False
            self.slice_axial.deleteToggle = False
        else:
            self.slice_axial.areaToggle = False

        if self.slice_coronal.areaToggle == False:
            self.slice_coronal.rulerToggle = False
            self.slice_coronal.tapeLineToggle = False
            self.slice_coronal.angleToggle = False
            self.slice_coronal.profileToggle = False
            self.slice_coronal.areaToggle = True
            self.slice_coronal.roiToggle = False
            self.slice_coronal.arrowToggle = False
            self.slice_coronal.noteToggle = False
            self.slice_coronal.deleteToggle = False
        else:
            self.slice_coronal.areaToggle = False

        if self.slice_sagittal.areaToggle == False:
            self.slice_sagittal.rulerToggle = False
            self.slice_sagittal.tapeLineToggle = False
            self.slice_sagittal.angleToggle = False
            self.slice_sagittal.profileToggle = False
            self.slice_sagittal.areaToggle = True
            self.slice_sagittal.roiToggle = False
            self.slice_sagittal.arrowToggle = False
            self.slice_sagittal.noteToggle = False
            self.slice_sagittal.deleteToggle = False
        else:
            self.slice_sagittal.areaToggle = False
        '''
        if self.volume.areaToggle == False:
            self.volume.areaToggle = True
        else:
            self.volume.areaToggle = False
        '''
    def menuRoiCheck(self):
        if self.slice_axial.roiToggle == False:
            self.slice_axial.rulerToggle = False
            self.slice_axial.tapeLineToggle = False
            self.slice_axial.angleToggle = False
            self.slice_axial.profileToggle = False
            self.slice_axial.areaToggle = False
            self.slice_axial.roiToggle = True
            self.slice_axial.arrowToggle = False
            self.slice_axial.noteToggle = False
            self.slice_axial.deleteToggle = False
        else:
            self.slice_axial.roiToggle = False

        if self.slice_coronal.roiToggle == False:
            self.slice_coronal.rulerToggle = False
            self.slice_coronal.tapeLineToggle = False
            self.slice_coronal.angleToggle = False
            self.slice_coronal.profileToggle = False
            self.slice_coronal.areaToggle = False
            self.slice_coronal.roiToggle = True
            self.slice_coronal.arrowToggle = False
            self.slice_coronal.noteToggle = False
            self.slice_coronal.deleteToggle = False
        else:
            self.slice_coronal.roiToggle = False

        if self.slice_sagittal.roiToggle == False:
            self.slice_sagittal.rulerToggle = False
            self.slice_sagittal.tapeLineToggle = False
            self.slice_sagittal.angleToggle = False
            self.slice_sagittal.profileToggle = False
            self.slice_sagittal.areaToggle = False
            self.slice_sagittal.roiToggle = True
            self.slice_sagittal.arrowToggle = False
            self.slice_sagittal.noteToggle = False
            self.slice_sagittal.deleteToggle = False
        else:
            self.slice_sagittal.roiToggle = False
        '''
        if self.volume.roiToggle == False:
            self.volume.roiToggle = True
        else:
            self.volume.roiToggle = False
        '''
    def menuArrowCheck(self):
        if self.slice_axial.arrowToggle == False:
            self.slice_axial.rulerToggle = False
            self.slice_axial.tapeLineToggle = False
            self.slice_axial.angleToggle = False
            self.slice_axial.profileToggle = False
            self.slice_axial.areaToggle = False
            self.slice_axial.roiToggle = False
            self.slice_axial.arrowToggle = True
            self.slice_axial.noteToggle = False
            self.slice_axial.deleteToggle = False
        else:
            self.slice_axial.arrowToggle = False

        if self.slice_coronal.arrowToggle == False:
            self.slice_coronal.rulerToggle = False
            self.slice_coronal.tapeLineToggle = False
            self.slice_coronal.angleToggle = False
            self.slice_coronal.profileToggle = False
            self.slice_coronal.areaToggle = False
            self.slice_coronal.roiToggle = False
            self.slice_coronal.arrowToggle = True
            self.slice_coronal.noteToggle = False
            self.slice_coronal.deleteToggle = False
        else:
            self.slice_coronal.arrowToggle = False

        if self.slice_sagittal.arrowToggle == False:
            self.slice_sagittal.rulerToggle = False
            self.slice_sagittal.tapeLineToggle = False
            self.slice_sagittal.angleToggle = False
            self.slice_sagittal.profileToggle = False
            self.slice_sagittal.areaToggle = False
            self.slice_sagittal.roiToggle = False
            self.slice_sagittal.arrowToggle = True
            self.slice_sagittal.noteToggle = False
            self.slice_sagittal.deleteToggle = False
        else:
            self.slice_sagittal.arrowToggle = False
        '''
        if self.volume.arrowToggle == False:
            self.volume.arrowToggle = True
        else:
            self.volume.arrowToggle = False
        '''
    def menuNoteCheck(self):
        if self.slice_axial.noteToggle == False:
            self.slice_axial.rulerToggle = False
            self.slice_axial.tapeLineToggle = False
            self.slice_axial.angleToggle = False
            self.slice_axial.profileToggle = False
            self.slice_axial.areaToggle = False
            self.slice_axial.roiToggle = False
            self.slice_axial.arrowToggle = False
            self.slice_axial.noteToggle = True
            self.slice_axial.deleteToggle = False
        else:
            self.slice_axial.noteToggle = False

        if self.slice_coronal.noteToggle == False:
            self.slice_coronal.rulerToggle = False
            self.slice_coronal.tapeLineToggle = False
            self.slice_coronal.angleToggle = False
            self.slice_coronal.profileToggle = False
            self.slice_coronal.areaToggle = False
            self.slice_coronal.roiToggle = False
            self.slice_coronal.arrowToggle = False
            self.slice_coronal.noteToggle = True
            self.slice_coronal.deleteToggle = False
        else:
            self.slice_coronal.noteToggle = False

        if self.slice_sagittal.noteToggle == False:
            self.slice_sagittal.rulerToggle = False
            self.slice_sagittal.tapeLineToggle = False
            self.slice_sagittal.angleToggle = False
            self.slice_sagittal.profileToggle = False
            self.slice_sagittal.areaToggle = False
            self.slice_sagittal.roiToggle = False
            self.slice_sagittal.arrowToggle = False
            self.slice_sagittal.noteToggle = True
            self.slice_sagittal.deleteToggle = False
        else:
            self.slice_sagittal.noteToggle = False
        '''
        if self.volume.noteToggle == False:
            self.volume.noteToggle = True
        else:
            self.volume.noteToggle = False
        '''
    def menuDeleteCheck(self):
        if self.slice_axial.deleteToggle == False:
            self.slice_axial.rulerToggle = False
            self.slice_axial.tapeLineToggle = False
            self.slice_axial.angleToggle = False
            self.slice_axial.profileToggle = False
            self.slice_axial.areaToggle = False
            self.slice_axial.roiToggle = False
            self.slice_axial.arrowToggle = False
            self.slice_axial.noteToggle = False
            self.slice_axial.deleteToggle = True
        else:
            self.slice_axial.deleteToggle = False

        if self.slice_coronal.deleteToggle == False:
            self.slice_coronal.rulerToggle = False
            self.slice_coronal.tapeLineToggle = False
            self.slice_coronal.angleToggle = False
            self.slice_coronal.profileToggle = False
            self.slice_coronal.areaToggle = False
            self.slice_coronal.roiToggle = False
            self.slice_coronal.arrowToggle = False
            self.slice_coronal.noteToggle = False
            self.slice_coronal.deleteToggle = True
        else:
            self.slice_coronal.deleteToggle = False

        if self.slice_sagittal.deleteToggle == False:
            self.slice_sagittal.rulerToggle = False
            self.slice_sagittal.tapeLineToggle = False
            self.slice_sagittal.angleToggle = False
            self.slice_sagittal.profileToggle = False
            self.slice_sagittal.areaToggle = False
            self.slice_sagittal.roiToggle = False
            self.slice_sagittal.arrowToggle = False
            self.slice_sagittal.noteToggle = False
            self.slice_sagittal.deleteToggle = True
        else:
            self.slice_sagittal.deleteToggle = False
        '''
        if self.volume.deleteToggle == False:
            self.volume.deleteToggle = True
        else:
            self.volume.deleteToggle = False
        '''
    def __init__(self):
        super().__init__()

        self.initMenu()
        self.frame = QFrame()
        self.gridLayout = QGridLayout()

        readDicom = ReadDIcom("./dicom")
        self.volume = VolumeScreen(readDicom)
        self.slice_axial = Slice(self.SliceOrientation.axial, readDicom)
        self.slice_coronal = Slice(self.SliceOrientation.coronal, readDicom)
        self.slice_sagittal = Slice(self.SliceOrientation.sagittal, readDicom)

        self.gridLayout.addWidget(self.slice_axial, 0, 0)
        self.gridLayout.addWidget(self.slice_coronal, 0, 1)
        self.gridLayout.addWidget(self.slice_sagittal, 1, 0)
        self.gridLayout.addWidget(self.volume, 1, 1)

        self.resize(1000, 500)
        self.frame.setLayout(self.gridLayout)
        self.setCentralWidget(self.frame)
        self.show()


if __name__ == '__main__':

    ex = MainWindow()
    _qapp.exec_()