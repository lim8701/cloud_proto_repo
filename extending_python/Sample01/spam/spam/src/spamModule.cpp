#include "../spamModule.h"

static PyObject *
spam_strlen(PyObject* self, PyObject *args)
{
	char* str;
	int len;
	if (!PyArg_ParseTuple(args, "s", &str))
		return NULL;

	len = strlen(str);
	return Py_BuildValue("i", len);
}

static PyObject*
spam_division(PyObject* self, PyObject* args)
{
	float quotient = 0;
	float dividend, divisor = 0;

	if (!PyArg_ParseTuple(args, "ff", &dividend, &divisor))
		return NULL;

	if (divisor){
		quotient = dividend / divisor;
	}
	else{
		PyErr_SetString(PyExc_ZeroDivisionError, "divisor must not be zero");
		return NULL;		
	}

	return Py_BuildValue("f", quotient);
}



static PyMethodDef SpamMethods[] = {
	{ "strlen", spam_strlen, METH_VARARGS,		"count a string length." },
	{ "division", spam_division, METH_VARARGS,  "divide integer." },
	{ NULL, NULL, 0, NULL }
};


static struct PyModuleDef SpamModule = {
	PyModuleDef_HEAD_INIT,
	"spam",
	"It is test module.",
	-1, SpamMethods
};


PyMODINIT_FUNC
PyInit_spam(void)
{
	return PyModule_Create(&SpamModule);
}